package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Course.
 */
@Entity
@Table(name = "course")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_course", nullable = false, unique = true)
    private Long idCourse;

    @NotNull
    @Column(name = "id_coursier", nullable = false)
    private Long idCoursier;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @JsonIgnoreProperties(value = { "produitCommandes", "paiement", "course", "utilisateur", "cooperative" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Commande commande;

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses" }, allowSetters = true)
    private Coursier coursier;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Course id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCourse() {
        return this.idCourse;
    }

    public Course idCourse(Long idCourse) {
        this.setIdCourse(idCourse);
        return this;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    public Long getIdCoursier() {
        return this.idCoursier;
    }

    public Course idCoursier(Long idCoursier) {
        this.setIdCoursier(idCoursier);
        return this;
    }

    public void setIdCoursier(Long idCoursier) {
        this.idCoursier = idCoursier;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Course adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Commande getCommande() {
        return this.commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Course commande(Commande commande) {
        this.setCommande(commande);
        return this;
    }

    public Coursier getCoursier() {
        return this.coursier;
    }

    public void setCoursier(Coursier coursier) {
        this.coursier = coursier;
    }

    public Course coursier(Coursier coursier) {
        this.setCoursier(coursier);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Course)) {
            return false;
        }
        return id != null && id.equals(((Course) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Course{" +
            "id=" + getId() +
            ", idCourse=" + getIdCourse() +
            ", idCoursier=" + getIdCoursier() +
            ", adresse='" + getAdresse() + "'" +
            "}";
    }
}
