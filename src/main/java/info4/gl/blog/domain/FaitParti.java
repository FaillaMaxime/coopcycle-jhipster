package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import info4.gl.blog.domain.enumeration.GERE;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A FaitParti.
 */
@Entity
@Table(name = "fait_parti")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FaitParti implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gere", nullable = false, unique = true)
    private GERE gere;

    @JsonIgnoreProperties(value = { "menus", "commandes", "faitParti" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Cooperative cooperative;

    @ManyToOne
    @JsonIgnoreProperties(value = { "commandes", "roles", "faitPartis" }, allowSetters = true)
    private Utilisateur utilisateur;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FaitParti id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GERE getGere() {
        return this.gere;
    }

    public FaitParti gere(GERE gere) {
        this.setGere(gere);
        return this;
    }

    public void setGere(GERE gere) {
        this.gere = gere;
    }

    public Cooperative getCooperative() {
        return this.cooperative;
    }

    public void setCooperative(Cooperative cooperative) {
        this.cooperative = cooperative;
    }

    public FaitParti cooperative(Cooperative cooperative) {
        this.setCooperative(cooperative);
        return this;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public FaitParti utilisateur(Utilisateur utilisateur) {
        this.setUtilisateur(utilisateur);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FaitParti)) {
            return false;
        }
        return id != null && id.equals(((FaitParti) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FaitParti{" +
            "id=" + getId() +
            ", gere='" + getGere() + "'" +
            "}";
    }
}
