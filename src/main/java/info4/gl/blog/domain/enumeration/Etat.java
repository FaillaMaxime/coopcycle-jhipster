package info4.gl.blog.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    PREPARATION,
    EMPORTE,
    ENCOURSDELIVRAISON,
    LIVRE,
    PAYE,
}
