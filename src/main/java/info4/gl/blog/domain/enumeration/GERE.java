package info4.gl.blog.domain.enumeration;

/**
 * The GERE enumeration.
 */
public enum GERE {
    DG,
    Societaire,
    ConseilAdministration,
    Admin,
}
