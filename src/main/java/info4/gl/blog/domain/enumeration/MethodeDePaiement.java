package info4.gl.blog.domain.enumeration;

/**
 * The MethodeDePaiement enumeration.
 */
public enum MethodeDePaiement {
    CB,
    Mastercard,
    Visa,
    Paypal,
    Apple,
    Pay,
    Google,
    Chequerepas,
    Bitcoin,
    Izly,
}
