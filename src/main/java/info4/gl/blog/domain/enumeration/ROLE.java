package info4.gl.blog.domain.enumeration;

/**
 * The ROLE enumeration.
 */
public enum ROLE {
    Utilisateur,
    Societaire,
    DG,
    Commercant,
    Coursier,
}
