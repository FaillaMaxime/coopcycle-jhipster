package info4.gl.blog.repository;

import info4.gl.blog.domain.FaitParti;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the FaitParti entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FaitPartiRepository extends JpaRepository<FaitParti, Long> {}
