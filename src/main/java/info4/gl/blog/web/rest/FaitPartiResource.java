package info4.gl.blog.web.rest;

import info4.gl.blog.domain.FaitParti;
import info4.gl.blog.repository.FaitPartiRepository;
import info4.gl.blog.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link info4.gl.blog.domain.FaitParti}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FaitPartiResource {

    private final Logger log = LoggerFactory.getLogger(FaitPartiResource.class);

    private static final String ENTITY_NAME = "faitParti";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FaitPartiRepository faitPartiRepository;

    public FaitPartiResource(FaitPartiRepository faitPartiRepository) {
        this.faitPartiRepository = faitPartiRepository;
    }

    /**
     * {@code POST  /fait-partis} : Create a new faitParti.
     *
     * @param faitParti the faitParti to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new faitParti, or with status {@code 400 (Bad Request)} if the faitParti has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fait-partis")
    public ResponseEntity<FaitParti> createFaitParti(@Valid @RequestBody FaitParti faitParti) throws URISyntaxException {
        log.debug("REST request to save FaitParti : {}", faitParti);
        if (faitParti.getId() != null) {
            throw new BadRequestAlertException("A new faitParti cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FaitParti result = faitPartiRepository.save(faitParti);
        return ResponseEntity
            .created(new URI("/api/fait-partis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fait-partis/:id} : Updates an existing faitParti.
     *
     * @param id the id of the faitParti to save.
     * @param faitParti the faitParti to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated faitParti,
     * or with status {@code 400 (Bad Request)} if the faitParti is not valid,
     * or with status {@code 500 (Internal Server Error)} if the faitParti couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fait-partis/{id}")
    public ResponseEntity<FaitParti> updateFaitParti(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody FaitParti faitParti
    ) throws URISyntaxException {
        log.debug("REST request to update FaitParti : {}, {}", id, faitParti);
        if (faitParti.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, faitParti.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!faitPartiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FaitParti result = faitPartiRepository.save(faitParti);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, faitParti.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /fait-partis/:id} : Partial updates given fields of an existing faitParti, field will ignore if it is null
     *
     * @param id the id of the faitParti to save.
     * @param faitParti the faitParti to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated faitParti,
     * or with status {@code 400 (Bad Request)} if the faitParti is not valid,
     * or with status {@code 404 (Not Found)} if the faitParti is not found,
     * or with status {@code 500 (Internal Server Error)} if the faitParti couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fait-partis/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FaitParti> partialUpdateFaitParti(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody FaitParti faitParti
    ) throws URISyntaxException {
        log.debug("REST request to partial update FaitParti partially : {}, {}", id, faitParti);
        if (faitParti.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, faitParti.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!faitPartiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FaitParti> result = faitPartiRepository
            .findById(faitParti.getId())
            .map(existingFaitParti -> {
                if (faitParti.getGere() != null) {
                    existingFaitParti.setGere(faitParti.getGere());
                }

                return existingFaitParti;
            })
            .map(faitPartiRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, faitParti.getId().toString())
        );
    }

    /**
     * {@code GET  /fait-partis} : get all the faitPartis.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of faitPartis in body.
     */
    @GetMapping("/fait-partis")
    public List<FaitParti> getAllFaitPartis() {
        log.debug("REST request to get all FaitPartis");
        return faitPartiRepository.findAll();
    }

    /**
     * {@code GET  /fait-partis/:id} : get the "id" faitParti.
     *
     * @param id the id of the faitParti to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the faitParti, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fait-partis/{id}")
    public ResponseEntity<FaitParti> getFaitParti(@PathVariable Long id) {
        log.debug("REST request to get FaitParti : {}", id);
        Optional<FaitParti> faitParti = faitPartiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(faitParti);
    }

    /**
     * {@code DELETE  /fait-partis/:id} : delete the "id" faitParti.
     *
     * @param id the id of the faitParti to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fait-partis/{id}")
    public ResponseEntity<Void> deleteFaitParti(@PathVariable Long id) {
        log.debug("REST request to delete FaitParti : {}", id);
        faitPartiRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
