import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFaitParti } from '@/shared/model/fait-parti.model';
import FaitPartiService from './fait-parti.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class FaitPartiDetails extends Vue {
  @Inject('faitPartiService') private faitPartiService: () => FaitPartiService;
  @Inject('alertService') private alertService: () => AlertService;

  public faitParti: IFaitParti = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.faitPartiId) {
        vm.retrieveFaitParti(to.params.faitPartiId);
      }
    });
  }

  public retrieveFaitParti(faitPartiId) {
    this.faitPartiService()
      .find(faitPartiId)
      .then(res => {
        this.faitParti = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
