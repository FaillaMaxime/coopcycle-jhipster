import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IFaitParti } from '@/shared/model/fait-parti.model';

import FaitPartiService from './fait-parti.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class FaitParti extends Vue {
  @Inject('faitPartiService') private faitPartiService: () => FaitPartiService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public faitPartis: IFaitParti[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllFaitPartis();
  }

  public clear(): void {
    this.retrieveAllFaitPartis();
  }

  public retrieveAllFaitPartis(): void {
    this.isFetching = true;
    this.faitPartiService()
      .retrieve()
      .then(
        res => {
          this.faitPartis = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IFaitParti): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeFaitParti(): void {
    this.faitPartiService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('blogApp.faitParti.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllFaitPartis();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
