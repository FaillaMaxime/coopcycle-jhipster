import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProduitCommande } from '@/shared/model/produit-commande.model';
import ProduitCommandeService from './produit-commande.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ProduitCommandeDetails extends Vue {
  @Inject('produitCommandeService') private produitCommandeService: () => ProduitCommandeService;
  @Inject('alertService') private alertService: () => AlertService;

  public produitCommande: IProduitCommande = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.produitCommandeId) {
        vm.retrieveProduitCommande(to.params.produitCommandeId);
      }
    });
  }

  public retrieveProduitCommande(produitCommandeId) {
    this.produitCommandeService()
      .find(produitCommandeId)
      .then(res => {
        this.produitCommande = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
