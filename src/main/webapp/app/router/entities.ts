import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

// prettier-ignore
const FaitParti = () => import('@/entities/fait-parti/fait-parti.vue');
// prettier-ignore
const FaitPartiUpdate = () => import('@/entities/fait-parti/fait-parti-update.vue');
// prettier-ignore
const FaitPartiDetails = () => import('@/entities/fait-parti/fait-parti-details.vue');
// prettier-ignore
const Role = () => import('@/entities/role/role.vue');
// prettier-ignore
const RoleUpdate = () => import('@/entities/role/role-update.vue');
// prettier-ignore
const RoleDetails = () => import('@/entities/role/role-details.vue');
// prettier-ignore
const Cooperative = () => import('@/entities/cooperative/cooperative.vue');
// prettier-ignore
const CooperativeUpdate = () => import('@/entities/cooperative/cooperative-update.vue');
// prettier-ignore
const CooperativeDetails = () => import('@/entities/cooperative/cooperative-details.vue');
// prettier-ignore
const Utilisateur = () => import('@/entities/utilisateur/utilisateur.vue');
// prettier-ignore
const UtilisateurUpdate = () => import('@/entities/utilisateur/utilisateur-update.vue');
// prettier-ignore
const UtilisateurDetails = () => import('@/entities/utilisateur/utilisateur-details.vue');
// prettier-ignore
const Coursier = () => import('@/entities/coursier/coursier.vue');
// prettier-ignore
const CoursierUpdate = () => import('@/entities/coursier/coursier-update.vue');
// prettier-ignore
const CoursierDetails = () => import('@/entities/coursier/coursier-details.vue');
// prettier-ignore
const Course = () => import('@/entities/course/course.vue');
// prettier-ignore
const CourseUpdate = () => import('@/entities/course/course-update.vue');
// prettier-ignore
const CourseDetails = () => import('@/entities/course/course-details.vue');
// prettier-ignore
const Menu = () => import('@/entities/menu/menu.vue');
// prettier-ignore
const MenuUpdate = () => import('@/entities/menu/menu-update.vue');
// prettier-ignore
const MenuDetails = () => import('@/entities/menu/menu-details.vue');
// prettier-ignore
const Commande = () => import('@/entities/commande/commande.vue');
// prettier-ignore
const CommandeUpdate = () => import('@/entities/commande/commande-update.vue');
// prettier-ignore
const CommandeDetails = () => import('@/entities/commande/commande-details.vue');
// prettier-ignore
const Produit = () => import('@/entities/produit/produit.vue');
// prettier-ignore
const ProduitUpdate = () => import('@/entities/produit/produit-update.vue');
// prettier-ignore
const ProduitDetails = () => import('@/entities/produit/produit-details.vue');
// prettier-ignore
const ProduitCommande = () => import('@/entities/produit-commande/produit-commande.vue');
// prettier-ignore
const ProduitCommandeUpdate = () => import('@/entities/produit-commande/produit-commande-update.vue');
// prettier-ignore
const ProduitCommandeDetails = () => import('@/entities/produit-commande/produit-commande-details.vue');
// prettier-ignore
const Paiement = () => import('@/entities/paiement/paiement.vue');
// prettier-ignore
const PaiementUpdate = () => import('@/entities/paiement/paiement-update.vue');
// prettier-ignore
const PaiementDetails = () => import('@/entities/paiement/paiement-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'fait-parti',
      name: 'FaitParti',
      component: FaitParti,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'fait-parti/new',
      name: 'FaitPartiCreate',
      component: FaitPartiUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'fait-parti/:faitPartiId/edit',
      name: 'FaitPartiEdit',
      component: FaitPartiUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'fait-parti/:faitPartiId/view',
      name: 'FaitPartiView',
      component: FaitPartiDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'role',
      name: 'Role',
      component: Role,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'role/new',
      name: 'RoleCreate',
      component: RoleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'role/:roleId/edit',
      name: 'RoleEdit',
      component: RoleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'role/:roleId/view',
      name: 'RoleView',
      component: RoleDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative',
      name: 'Cooperative',
      component: Cooperative,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/new',
      name: 'CooperativeCreate',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/edit',
      name: 'CooperativeEdit',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/view',
      name: 'CooperativeView',
      component: CooperativeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'utilisateur',
      name: 'Utilisateur',
      component: Utilisateur,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'utilisateur/new',
      name: 'UtilisateurCreate',
      component: UtilisateurUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'utilisateur/:utilisateurId/edit',
      name: 'UtilisateurEdit',
      component: UtilisateurUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'utilisateur/:utilisateurId/view',
      name: 'UtilisateurView',
      component: UtilisateurDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier',
      name: 'Coursier',
      component: Coursier,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/new',
      name: 'CoursierCreate',
      component: CoursierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/:coursierId/edit',
      name: 'CoursierEdit',
      component: CoursierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/:coursierId/view',
      name: 'CoursierView',
      component: CoursierDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course',
      name: 'Course',
      component: Course,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/new',
      name: 'CourseCreate',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/edit',
      name: 'CourseEdit',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/view',
      name: 'CourseView',
      component: CourseDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu',
      name: 'Menu',
      component: Menu,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/new',
      name: 'MenuCreate',
      component: MenuUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/:menuId/edit',
      name: 'MenuEdit',
      component: MenuUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/:menuId/view',
      name: 'MenuView',
      component: MenuDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande',
      name: 'Commande',
      component: Commande,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/new',
      name: 'CommandeCreate',
      component: CommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/:commandeId/edit',
      name: 'CommandeEdit',
      component: CommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/:commandeId/view',
      name: 'CommandeView',
      component: CommandeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit',
      name: 'Produit',
      component: Produit,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/new',
      name: 'ProduitCreate',
      component: ProduitUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/:produitId/edit',
      name: 'ProduitEdit',
      component: ProduitUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/:produitId/view',
      name: 'ProduitView',
      component: ProduitDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande',
      name: 'ProduitCommande',
      component: ProduitCommande,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/new',
      name: 'ProduitCommandeCreate',
      component: ProduitCommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/:produitCommandeId/edit',
      name: 'ProduitCommandeEdit',
      component: ProduitCommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/:produitCommandeId/view',
      name: 'ProduitCommandeView',
      component: ProduitCommandeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement',
      name: 'Paiement',
      component: Paiement,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/new',
      name: 'PaiementCreate',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/edit',
      name: 'PaiementEdit',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/view',
      name: 'PaiementView',
      component: PaiementDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
