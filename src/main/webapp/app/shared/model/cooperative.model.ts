import { IMenu } from '@/shared/model/menu.model';
import { ICommande } from '@/shared/model/commande.model';
import { IFaitParti } from '@/shared/model/fait-parti.model';

export interface ICooperative {
  id?: number;
  nom?: string | null;
  adresse?: string;
  menus?: IMenu[] | null;
  commandes?: ICommande[] | null;
  faitParti?: IFaitParti | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public nom?: string | null,
    public adresse?: string,
    public menus?: IMenu[] | null,
    public commandes?: ICommande[] | null,
    public faitParti?: IFaitParti | null
  ) {}
}
