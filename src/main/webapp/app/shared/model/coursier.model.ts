import { ICourse } from '@/shared/model/course.model';

export interface ICoursier {
  id?: number;
  nom?: string | null;
  prenom?: string | null;
  telephone?: string | null;
  vehicule?: string | null;
  courses?: ICourse[] | null;
}

export class Coursier implements ICoursier {
  constructor(
    public id?: number,
    public nom?: string | null,
    public prenom?: string | null,
    public telephone?: string | null,
    public vehicule?: string | null,
    public courses?: ICourse[] | null
  ) {}
}
