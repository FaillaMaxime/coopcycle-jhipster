export enum MethodeDePaiement {
  CB = 'CB',

  Mastercard = 'Mastercard',

  Visa = 'Visa',

  Paypal = 'Paypal',

  Apple = 'Apple',

  Pay = 'Pay',

  Google = 'Google',

  Chequerepas = 'Chequerepas',

  Bitcoin = 'Bitcoin',

  Izly = 'Izly',
}
