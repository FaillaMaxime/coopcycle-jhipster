export enum ROLE {
  Utilisateur = 'Utilisateur',

  Societaire = 'Societaire',

  DG = 'DG',

  Commercant = 'Commercant',

  Coursier = 'Coursier',
}
