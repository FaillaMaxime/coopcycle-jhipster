import { ICooperative } from '@/shared/model/cooperative.model';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

import { GERE } from '@/shared/model/enumerations/gere.model';
export interface IFaitParti {
  id?: number;
  gere?: GERE;
  cooperative?: ICooperative | null;
  utilisateur?: IUtilisateur | null;
}

export class FaitParti implements IFaitParti {
  constructor(public id?: number, public gere?: GERE, public cooperative?: ICooperative | null, public utilisateur?: IUtilisateur | null) {}
}
