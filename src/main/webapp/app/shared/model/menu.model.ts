import { IProduit } from '@/shared/model/produit.model';
import { ICooperative } from '@/shared/model/cooperative.model';

export interface IMenu {
  id?: number;
  idMenu?: number;
  idCooperative?: number;
  produits?: IProduit[] | null;
  cooperative?: ICooperative | null;
}

export class Menu implements IMenu {
  constructor(
    public id?: number,
    public idMenu?: number,
    public idCooperative?: number,
    public produits?: IProduit[] | null,
    public cooperative?: ICooperative | null
  ) {}
}
