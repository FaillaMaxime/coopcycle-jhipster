import { ICommande } from '@/shared/model/commande.model';

import { MethodeDePaiement } from '@/shared/model/enumerations/methode-de-paiement.model';
export interface IPaiement {
  id?: number;
  idPaiement?: number | null;
  idCommande?: number | null;
  methodeDePaiement?: MethodeDePaiement;
  commande?: ICommande | null;
}

export class Paiement implements IPaiement {
  constructor(
    public id?: number,
    public idPaiement?: number | null,
    public idCommande?: number | null,
    public methodeDePaiement?: MethodeDePaiement,
    public commande?: ICommande | null
  ) {}
}
