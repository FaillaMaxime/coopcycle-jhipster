import { IUtilisateur } from '@/shared/model/utilisateur.model';

import { ROLE } from '@/shared/model/enumerations/role.model';
export interface IRole {
  id?: number;
  role?: ROLE;
  utilisateur?: IUtilisateur | null;
}

export class Role implements IRole {
  constructor(public id?: number, public role?: ROLE, public utilisateur?: IUtilisateur | null) {}
}
