import { ICommande } from '@/shared/model/commande.model';
import { IRole } from '@/shared/model/role.model';
import { IFaitParti } from '@/shared/model/fait-parti.model';

export interface IUtilisateur {
  id?: number;
  nom?: string | null;
  prenom?: string | null;
  telephone?: string | null;
  adresse?: string;
  commandes?: ICommande[] | null;
  roles?: IRole[] | null;
  faitPartis?: IFaitParti[] | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public nom?: string | null,
    public prenom?: string | null,
    public telephone?: string | null,
    public adresse?: string,
    public commandes?: ICommande[] | null,
    public roles?: IRole[] | null,
    public faitPartis?: IFaitParti[] | null
  ) {}
}
