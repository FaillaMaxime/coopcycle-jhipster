package info4.gl.blog.domain;

import static org.assertj.core.api.Assertions.assertThat;

import info4.gl.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FaitPartiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FaitParti.class);
        FaitParti faitParti1 = new FaitParti();
        faitParti1.setId(1L);
        FaitParti faitParti2 = new FaitParti();
        faitParti2.setId(faitParti1.getId());
        assertThat(faitParti1).isEqualTo(faitParti2);
        faitParti2.setId(2L);
        assertThat(faitParti1).isNotEqualTo(faitParti2);
        faitParti1.setId(null);
        assertThat(faitParti1).isNotEqualTo(faitParti2);
    }
}
