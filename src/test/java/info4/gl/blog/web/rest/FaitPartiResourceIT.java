package info4.gl.blog.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import info4.gl.blog.IntegrationTest;
import info4.gl.blog.domain.FaitParti;
import info4.gl.blog.domain.enumeration.GERE;
import info4.gl.blog.repository.FaitPartiRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FaitPartiResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FaitPartiResourceIT {

    private static final GERE DEFAULT_GERE = GERE.DG;
    private static final GERE UPDATED_GERE = GERE.Societaire;

    private static final String ENTITY_API_URL = "/api/fait-partis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FaitPartiRepository faitPartiRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFaitPartiMockMvc;

    private FaitParti faitParti;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaitParti createEntity(EntityManager em) {
        FaitParti faitParti = new FaitParti().gere(DEFAULT_GERE);
        return faitParti;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaitParti createUpdatedEntity(EntityManager em) {
        FaitParti faitParti = new FaitParti().gere(UPDATED_GERE);
        return faitParti;
    }

    @BeforeEach
    public void initTest() {
        faitParti = createEntity(em);
    }

    @Test
    @Transactional
    void createFaitParti() throws Exception {
        int databaseSizeBeforeCreate = faitPartiRepository.findAll().size();
        // Create the FaitParti
        restFaitPartiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faitParti)))
            .andExpect(status().isCreated());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeCreate + 1);
        FaitParti testFaitParti = faitPartiList.get(faitPartiList.size() - 1);
        assertThat(testFaitParti.getGere()).isEqualTo(DEFAULT_GERE);
    }

    @Test
    @Transactional
    void createFaitPartiWithExistingId() throws Exception {
        // Create the FaitParti with an existing ID
        faitParti.setId(1L);

        int databaseSizeBeforeCreate = faitPartiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFaitPartiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faitParti)))
            .andExpect(status().isBadRequest());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkGereIsRequired() throws Exception {
        int databaseSizeBeforeTest = faitPartiRepository.findAll().size();
        // set the field null
        faitParti.setGere(null);

        // Create the FaitParti, which fails.

        restFaitPartiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faitParti)))
            .andExpect(status().isBadRequest());

        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFaitPartis() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        // Get all the faitPartiList
        restFaitPartiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(faitParti.getId().intValue())))
            .andExpect(jsonPath("$.[*].gere").value(hasItem(DEFAULT_GERE.toString())));
    }

    @Test
    @Transactional
    void getFaitParti() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        // Get the faitParti
        restFaitPartiMockMvc
            .perform(get(ENTITY_API_URL_ID, faitParti.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(faitParti.getId().intValue()))
            .andExpect(jsonPath("$.gere").value(DEFAULT_GERE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingFaitParti() throws Exception {
        // Get the faitParti
        restFaitPartiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingFaitParti() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();

        // Update the faitParti
        FaitParti updatedFaitParti = faitPartiRepository.findById(faitParti.getId()).get();
        // Disconnect from session so that the updates on updatedFaitParti are not directly saved in db
        em.detach(updatedFaitParti);
        updatedFaitParti.gere(UPDATED_GERE);

        restFaitPartiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFaitParti.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFaitParti))
            )
            .andExpect(status().isOk());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
        FaitParti testFaitParti = faitPartiList.get(faitPartiList.size() - 1);
        assertThat(testFaitParti.getGere()).isEqualTo(UPDATED_GERE);
    }

    @Test
    @Transactional
    void putNonExistingFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, faitParti.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(faitParti))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(faitParti))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faitParti)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFaitPartiWithPatch() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();

        // Update the faitParti using partial update
        FaitParti partialUpdatedFaitParti = new FaitParti();
        partialUpdatedFaitParti.setId(faitParti.getId());

        partialUpdatedFaitParti.gere(UPDATED_GERE);

        restFaitPartiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFaitParti.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFaitParti))
            )
            .andExpect(status().isOk());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
        FaitParti testFaitParti = faitPartiList.get(faitPartiList.size() - 1);
        assertThat(testFaitParti.getGere()).isEqualTo(UPDATED_GERE);
    }

    @Test
    @Transactional
    void fullUpdateFaitPartiWithPatch() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();

        // Update the faitParti using partial update
        FaitParti partialUpdatedFaitParti = new FaitParti();
        partialUpdatedFaitParti.setId(faitParti.getId());

        partialUpdatedFaitParti.gere(UPDATED_GERE);

        restFaitPartiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFaitParti.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFaitParti))
            )
            .andExpect(status().isOk());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
        FaitParti testFaitParti = faitPartiList.get(faitPartiList.size() - 1);
        assertThat(testFaitParti.getGere()).isEqualTo(UPDATED_GERE);
    }

    @Test
    @Transactional
    void patchNonExistingFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, faitParti.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(faitParti))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(faitParti))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFaitParti() throws Exception {
        int databaseSizeBeforeUpdate = faitPartiRepository.findAll().size();
        faitParti.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaitPartiMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(faitParti))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FaitParti in the database
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFaitParti() throws Exception {
        // Initialize the database
        faitPartiRepository.saveAndFlush(faitParti);

        int databaseSizeBeforeDelete = faitPartiRepository.findAll().size();

        // Delete the faitParti
        restFaitPartiMockMvc
            .perform(delete(ENTITY_API_URL_ID, faitParti.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FaitParti> faitPartiList = faitPartiRepository.findAll();
        assertThat(faitPartiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
