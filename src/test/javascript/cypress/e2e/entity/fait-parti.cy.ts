import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('FaitParti e2e test', () => {
  const faitPartiPageUrl = '/fait-parti';
  const faitPartiPageUrlPattern = new RegExp('/fait-parti(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const faitPartiSample = {};

  let faitParti;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/fait-partis+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/fait-partis').as('postEntityRequest');
    cy.intercept('DELETE', '/api/fait-partis/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (faitParti) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/fait-partis/${faitParti.id}`,
      }).then(() => {
        faitParti = undefined;
      });
    }
  });

  it('FaitPartis menu should load FaitPartis page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fait-parti');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('FaitParti').should('exist');
    cy.url().should('match', faitPartiPageUrlPattern);
  });

  describe('FaitParti page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(faitPartiPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create FaitParti page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/fait-parti/new$'));
        cy.getEntityCreateUpdateHeading('FaitParti');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', faitPartiPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/fait-partis',
          body: faitPartiSample,
        }).then(({ body }) => {
          faitParti = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/fait-partis+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [faitParti],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(faitPartiPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details FaitParti page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('faitParti');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', faitPartiPageUrlPattern);
      });

      it('edit button click should load edit FaitParti page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('FaitParti');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', faitPartiPageUrlPattern);
      });

      it('edit button click should load edit FaitParti page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('FaitParti');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', faitPartiPageUrlPattern);
      });

      it('last delete button click should delete instance of FaitParti', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('faitParti').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', faitPartiPageUrlPattern);

        faitParti = undefined;
      });
    });
  });

  describe('new FaitParti page', () => {
    beforeEach(() => {
      cy.visit(`${faitPartiPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('FaitParti');
    });

    it('should create an instance of FaitParti', () => {
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        faitParti = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', faitPartiPageUrlPattern);
    });
  });
});
