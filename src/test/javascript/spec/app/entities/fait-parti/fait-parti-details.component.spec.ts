/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import FaitPartiDetailComponent from '@/entities/fait-parti/fait-parti-details.vue';
import FaitPartiClass from '@/entities/fait-parti/fait-parti-details.component';
import FaitPartiService from '@/entities/fait-parti/fait-parti.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('FaitParti Management Detail Component', () => {
    let wrapper: Wrapper<FaitPartiClass>;
    let comp: FaitPartiClass;
    let faitPartiServiceStub: SinonStubbedInstance<FaitPartiService>;

    beforeEach(() => {
      faitPartiServiceStub = sinon.createStubInstance<FaitPartiService>(FaitPartiService);

      wrapper = shallowMount<FaitPartiClass>(FaitPartiDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { faitPartiService: () => faitPartiServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFaitParti = { id: 123 };
        faitPartiServiceStub.find.resolves(foundFaitParti);

        // WHEN
        comp.retrieveFaitParti(123);
        await comp.$nextTick();

        // THEN
        expect(comp.faitParti).toBe(foundFaitParti);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFaitParti = { id: 123 };
        faitPartiServiceStub.find.resolves(foundFaitParti);

        // WHEN
        comp.beforeRouteEnter({ params: { faitPartiId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.faitParti).toBe(foundFaitParti);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
