/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import FaitPartiComponent from '@/entities/fait-parti/fait-parti.vue';
import FaitPartiClass from '@/entities/fait-parti/fait-parti.component';
import FaitPartiService from '@/entities/fait-parti/fait-parti.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('FaitParti Management Component', () => {
    let wrapper: Wrapper<FaitPartiClass>;
    let comp: FaitPartiClass;
    let faitPartiServiceStub: SinonStubbedInstance<FaitPartiService>;

    beforeEach(() => {
      faitPartiServiceStub = sinon.createStubInstance<FaitPartiService>(FaitPartiService);
      faitPartiServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<FaitPartiClass>(FaitPartiComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          faitPartiService: () => faitPartiServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      faitPartiServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllFaitPartis();
      await comp.$nextTick();

      // THEN
      expect(faitPartiServiceStub.retrieve.called).toBeTruthy();
      expect(comp.faitPartis[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      faitPartiServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(faitPartiServiceStub.retrieve.callCount).toEqual(1);

      comp.removeFaitParti();
      await comp.$nextTick();

      // THEN
      expect(faitPartiServiceStub.delete.called).toBeTruthy();
      expect(faitPartiServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
