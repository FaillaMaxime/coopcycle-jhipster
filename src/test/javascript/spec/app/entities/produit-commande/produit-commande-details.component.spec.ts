/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ProduitCommandeDetailComponent from '@/entities/produit-commande/produit-commande-details.vue';
import ProduitCommandeClass from '@/entities/produit-commande/produit-commande-details.component';
import ProduitCommandeService from '@/entities/produit-commande/produit-commande.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ProduitCommande Management Detail Component', () => {
    let wrapper: Wrapper<ProduitCommandeClass>;
    let comp: ProduitCommandeClass;
    let produitCommandeServiceStub: SinonStubbedInstance<ProduitCommandeService>;

    beforeEach(() => {
      produitCommandeServiceStub = sinon.createStubInstance<ProduitCommandeService>(ProduitCommandeService);

      wrapper = shallowMount<ProduitCommandeClass>(ProduitCommandeDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { produitCommandeService: () => produitCommandeServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundProduitCommande = { id: 123 };
        produitCommandeServiceStub.find.resolves(foundProduitCommande);

        // WHEN
        comp.retrieveProduitCommande(123);
        await comp.$nextTick();

        // THEN
        expect(comp.produitCommande).toBe(foundProduitCommande);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundProduitCommande = { id: 123 };
        produitCommandeServiceStub.find.resolves(foundProduitCommande);

        // WHEN
        comp.beforeRouteEnter({ params: { produitCommandeId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.produitCommande).toBe(foundProduitCommande);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
