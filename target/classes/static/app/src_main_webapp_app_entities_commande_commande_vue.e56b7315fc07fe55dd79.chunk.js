"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_commande_commande_vue"],{

/***/ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts&":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts& ***!
  \********************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-property-decorator */ "./node_modules/vue-property-decorator/lib/index.js");
/* harmony import */ var vue2_filters__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-filters */ "./node_modules/vue2-filters/dist/vue2-filters.js");
/* harmony import */ var vue2_filters__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_filters__WEBPACK_IMPORTED_MODULE_1__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Commande = /** @class */ (function (_super) {
    __extends(Commande, _super);
    function Commande() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.removeId = null;
        _this.commandes = [];
        _this.isFetching = false;
        return _this;
    }
    Commande.prototype.mounted = function () {
        this.retrieveAllCommandes();
    };
    Commande.prototype.clear = function () {
        this.retrieveAllCommandes();
    };
    Commande.prototype.retrieveAllCommandes = function () {
        var _this = this;
        this.isFetching = true;
        this.commandeService()
            .retrieve()
            .then(function (res) {
            _this.commandes = res.data;
            _this.isFetching = false;
        }, function (err) {
            _this.isFetching = false;
            _this.alertService().showHttpError(_this, err.response);
        });
    };
    Commande.prototype.handleSyncList = function () {
        this.clear();
    };
    Commande.prototype.prepareRemove = function (instance) {
        this.removeId = instance.id;
        if (this.$refs.removeEntity) {
            this.$refs.removeEntity.show();
        }
    };
    Commande.prototype.removeCommande = function () {
        var _this = this;
        this.commandeService()
            .delete(this.removeId)
            .then(function () {
            var message = _this.$t('blogApp.commande.deleted', { param: _this.removeId });
            _this.$bvToast.toast(message.toString(), {
                toaster: 'b-toaster-top-center',
                title: 'Info',
                variant: 'danger',
                solid: true,
                autoHideDelay: 5000,
            });
            _this.removeId = null;
            _this.retrieveAllCommandes();
            _this.closeDialog();
        })
            .catch(function (error) {
            _this.alertService().showHttpError(_this, error.response);
        });
    };
    Commande.prototype.closeDialog = function () {
        this.$refs.removeEntity.hide();
    };
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('commandeService'),
        __metadata("design:type", Function)
    ], Commande.prototype, "commandeService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('alertService'),
        __metadata("design:type", Function)
    ], Commande.prototype, "alertService", void 0);
    Commande = __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Component)({
            mixins: [(vue2_filters__WEBPACK_IMPORTED_MODULE_1___default().mixin)],
        })
    ], Commande);
    return Commande;
}(vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Vue));
/* harmony default export */ __webpack_exports__["default"] = (Commande);


/***/ }),

/***/ "./src/main/webapp/app/entities/commande/commande.vue":
/*!************************************************************!*\
  !*** ./src/main/webapp/app/entities/commande/commande.vue ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./commande.vue?vue&type=template&id=dddc90d8& */ "./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8&");
/* harmony import */ var _commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./commande.component.ts?vue&type=script&lang=ts& */ "./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__.render,
  _commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/main/webapp/app/entities/commande/commande.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts&":
/*!**********************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts& ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./commande.component.ts?vue&type=script&lang=ts& */ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/commande/commande.component.ts?vue&type=script&lang=ts&");
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8&":
/*!*******************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8& ***!
  \*******************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__.render; },
/* harmony export */   "staticRenderFns": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns; }
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_commande_vue_vue_type_template_id_dddc90d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./commande.vue?vue&type=template&id=dddc90d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/commande/commande.vue?vue&type=template&id=dddc90d8& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; },
/* harmony export */   "staticRenderFns": function() { return /* binding */ staticRenderFns; }
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c,
    _setup = _vm._self._setupProxy
  return _c(
    "div",
    [
      _c(
        "h2",
        { attrs: { id: "page-heading", "data-cy": "CommandeHeading" } },
        [
          _c(
            "span",
            {
              attrs: { id: "commande-heading" },
              domProps: {
                textContent: _vm._s(_vm.$t("blogApp.commande.home.title")),
              },
            },
            [_vm._v("Commandes")]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-flex justify-content-end" },
            [
              _c(
                "button",
                {
                  staticClass: "btn btn-info mr-2",
                  attrs: { disabled: _vm.isFetching },
                  on: { click: _vm.handleSyncList },
                },
                [
                  _c("font-awesome-icon", {
                    attrs: { icon: "sync", spin: _vm.isFetching },
                  }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      domProps: {
                        textContent: _vm._s(
                          _vm.$t("blogApp.commande.home.refreshListLabel")
                        ),
                      },
                    },
                    [_vm._v("Refresh List")]
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c("router-link", {
                attrs: { to: { name: "CommandeCreate" }, custom: "" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function ({ navigate }) {
                      return [
                        _c(
                          "button",
                          {
                            staticClass:
                              "btn btn-primary jh-create-entity create-commande",
                            attrs: {
                              id: "jh-create-entity",
                              "data-cy": "entityCreateButton",
                            },
                            on: { click: navigate },
                          },
                          [
                            _c("font-awesome-icon", {
                              attrs: { icon: "plus" },
                            }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                domProps: {
                                  textContent: _vm._s(
                                    _vm.$t("blogApp.commande.home.createLabel")
                                  ),
                                },
                              },
                              [_vm._v(" Create a new Commande ")]
                            ),
                          ],
                          1
                        ),
                      ]
                    },
                  },
                ]),
              }),
            ],
            1
          ),
        ]
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      !_vm.isFetching && _vm.commandes && _vm.commandes.length === 0
        ? _c("div", { staticClass: "alert alert-warning" }, [
            _c(
              "span",
              {
                domProps: {
                  textContent: _vm._s(_vm.$t("blogApp.commande.home.notFound")),
                },
              },
              [_vm._v("No commandes found")]
            ),
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.commandes && _vm.commandes.length > 0
        ? _c("div", { staticClass: "table-responsive" }, [
            _c(
              "table",
              {
                staticClass: "table table-striped",
                attrs: { "aria-describedby": "commandes" },
              },
              [
                _c("thead", [
                  _c("tr", [
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(_vm.$t("global.field.id")),
                          },
                        },
                        [_vm._v("ID")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.idCommande")
                            ),
                          },
                        },
                        [_vm._v("Id Commande")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.idCooperative")
                            ),
                          },
                        },
                        [_vm._v("Id Cooperative")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.idClient")
                            ),
                          },
                        },
                        [_vm._v("Id Client")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.idLivreur")
                            ),
                          },
                        },
                        [_vm._v("Id Livreur")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.idCourse")
                            ),
                          },
                        },
                        [_vm._v("Id Course")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.prix")
                            ),
                          },
                        },
                        [_vm._v("Prix")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.date")
                            ),
                          },
                        },
                        [_vm._v("Date")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.etat")
                            ),
                          },
                        },
                        [_vm._v("Etat")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.utilisateur")
                            ),
                          },
                        },
                        [_vm._v("Utilisateur")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.commande.cooperative")
                            ),
                          },
                        },
                        [_vm._v("Cooperative")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }),
                  ]),
                ]),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.commandes, function (commande) {
                    return _c(
                      "tr",
                      { key: commande.id, attrs: { "data-cy": "entityTable" } },
                      [
                        _c(
                          "td",
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "CommandeView",
                                    params: { commandeId: commande.id },
                                  },
                                },
                              },
                              [_vm._v(_vm._s(commande.id))]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.idCommande))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.idCooperative))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.idClient))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.idLivreur))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.idCourse))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(commande.prix))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(
                            _vm._s(
                              commande.date
                                ? _vm.$d(Date.parse(commande.date), "short")
                                : ""
                            )
                          ),
                        ]),
                        _vm._v(" "),
                        _c(
                          "td",
                          {
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("blogApp.Etat." + commande.etat)
                              ),
                            },
                          },
                          [_vm._v(_vm._s(commande.etat))]
                        ),
                        _vm._v(" "),
                        _c("td", [
                          commande.utilisateur
                            ? _c(
                                "div",
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "UtilisateurView",
                                          params: {
                                            utilisateurId:
                                              commande.utilisateur.id,
                                          },
                                        },
                                      },
                                    },
                                    [_vm._v(_vm._s(commande.utilisateur.id))]
                                  ),
                                ],
                                1
                              )
                            : _vm._e(),
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          commande.cooperative
                            ? _c(
                                "div",
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "CooperativeView",
                                          params: {
                                            cooperativeId:
                                              commande.cooperative.id,
                                          },
                                        },
                                      },
                                    },
                                    [_vm._v(_vm._s(commande.cooperative.id))]
                                  ),
                                ],
                                1
                              )
                            : _vm._e(),
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "text-right" }, [
                          _c(
                            "div",
                            { staticClass: "btn-group" },
                            [
                              _c("router-link", {
                                attrs: {
                                  to: {
                                    name: "CommandeView",
                                    params: { commandeId: commande.id },
                                  },
                                  custom: "",
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "default",
                                      fn: function ({ navigate }) {
                                        return [
                                          _c(
                                            "button",
                                            {
                                              staticClass:
                                                "btn btn-info btn-sm details",
                                              attrs: {
                                                "data-cy":
                                                  "entityDetailsButton",
                                              },
                                              on: { click: navigate },
                                            },
                                            [
                                              _c("font-awesome-icon", {
                                                attrs: { icon: "eye" },
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "d-none d-md-inline",
                                                  domProps: {
                                                    textContent: _vm._s(
                                                      _vm.$t(
                                                        "entity.action.view"
                                                      )
                                                    ),
                                                  },
                                                },
                                                [_vm._v("View")]
                                              ),
                                            ],
                                            1
                                          ),
                                        ]
                                      },
                                    },
                                  ],
                                  null,
                                  true
                                ),
                              }),
                              _vm._v(" "),
                              _c("router-link", {
                                attrs: {
                                  to: {
                                    name: "CommandeEdit",
                                    params: { commandeId: commande.id },
                                  },
                                  custom: "",
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "default",
                                      fn: function ({ navigate }) {
                                        return [
                                          _c(
                                            "button",
                                            {
                                              staticClass:
                                                "btn btn-primary btn-sm edit",
                                              attrs: {
                                                "data-cy": "entityEditButton",
                                              },
                                              on: { click: navigate },
                                            },
                                            [
                                              _c("font-awesome-icon", {
                                                attrs: { icon: "pencil-alt" },
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "d-none d-md-inline",
                                                  domProps: {
                                                    textContent: _vm._s(
                                                      _vm.$t(
                                                        "entity.action.edit"
                                                      )
                                                    ),
                                                  },
                                                },
                                                [_vm._v("Edit")]
                                              ),
                                            ],
                                            1
                                          ),
                                        ]
                                      },
                                    },
                                  ],
                                  null,
                                  true
                                ),
                              }),
                              _vm._v(" "),
                              _c(
                                "b-button",
                                {
                                  directives: [
                                    {
                                      name: "b-modal",
                                      rawName: "v-b-modal.removeEntity",
                                      modifiers: { removeEntity: true },
                                    },
                                  ],
                                  staticClass: "btn btn-sm",
                                  attrs: {
                                    variant: "danger",
                                    "data-cy": "entityDeleteButton",
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.prepareRemove(commande)
                                    },
                                  },
                                },
                                [
                                  _c("font-awesome-icon", {
                                    attrs: { icon: "times" },
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass: "d-none d-md-inline",
                                      domProps: {
                                        textContent: _vm._s(
                                          _vm.$t("entity.action.delete")
                                        ),
                                      },
                                    },
                                    [_vm._v("Delete")]
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ]),
                      ]
                    )
                  }),
                  0
                ),
              ]
            ),
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("b-modal", { ref: "removeEntity", attrs: { id: "removeEntity" } }, [
        _c("span", { attrs: { slot: "modal-title" }, slot: "modal-title" }, [
          _c(
            "span",
            {
              attrs: {
                id: "blogApp.commande.delete.question",
                "data-cy": "commandeDeleteDialogHeading",
              },
              domProps: { textContent: _vm._s(_vm.$t("entity.delete.title")) },
            },
            [_vm._v("Confirm delete operation")]
          ),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "modal-body" }, [
          _c(
            "p",
            {
              attrs: { id: "jhi-delete-commande-heading" },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.commande.delete.question", {
                    id: _vm.removeId,
                  })
                ),
              },
            },
            [
              _vm._v(
                "\n        Are you sure you want to delete this Commande?\n      "
              ),
            ]
          ),
        ]),
        _vm._v(" "),
        _c("div", { attrs: { slot: "modal-footer" }, slot: "modal-footer" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-secondary",
              attrs: { type: "button" },
              domProps: { textContent: _vm._s(_vm.$t("entity.action.cancel")) },
              on: {
                click: function ($event) {
                  return _vm.closeDialog()
                },
              },
            },
            [_vm._v("Cancel")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-primary",
              attrs: {
                type: "button",
                id: "jhi-confirm-delete-commande",
                "data-cy": "entityConfirmDeleteButton",
              },
              domProps: { textContent: _vm._s(_vm.$t("entity.action.delete")) },
              on: {
                click: function ($event) {
                  return _vm.removeCommande()
                },
              },
            },
            [_vm._v("\n        Delete\n      ")]
          ),
        ]),
      ]),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_commande_commande_vue.e56b7315fc07fe55dd79.chunk.js.map