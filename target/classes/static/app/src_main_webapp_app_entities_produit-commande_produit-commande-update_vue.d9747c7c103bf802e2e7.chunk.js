"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_produit-commande_produit-commande-update_vue"],{

/***/ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts& ***!
  \*******************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-property-decorator */ "./node_modules/vue-property-decorator/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var _shared_model_produit_commande_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/shared/model/produit-commande.model */ "./src/main/webapp/app/shared/model/produit-commande.model.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var validations = {
    produitCommande: {
        idProduit: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
            numeric: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.numeric,
        },
        idCommande: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
            numeric: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.numeric,
        },
        quantite: {},
        quantitePossible: {},
        products: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
        },
    },
};
var ProduitCommandeUpdate = /** @class */ (function (_super) {
    __extends(ProduitCommandeUpdate, _super);
    function ProduitCommandeUpdate() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.produitCommande = new _shared_model_produit_commande_model__WEBPACK_IMPORTED_MODULE_1__.ProduitCommande();
        _this.produits = [];
        _this.commandes = [];
        _this.isSaving = false;
        _this.currentLanguage = '';
        return _this;
    }
    ProduitCommandeUpdate.prototype.beforeRouteEnter = function (to, from, next) {
        next(function (vm) {
            if (to.params.produitCommandeId) {
                vm.retrieveProduitCommande(to.params.produitCommandeId);
            }
            vm.initRelationships();
        });
    };
    ProduitCommandeUpdate.prototype.created = function () {
        var _this = this;
        this.currentLanguage = this.$store.getters.currentLanguage;
        this.$store.watch(function () { return _this.$store.getters.currentLanguage; }, function () {
            _this.currentLanguage = _this.$store.getters.currentLanguage;
        });
        this.produitCommande.products = [];
    };
    ProduitCommandeUpdate.prototype.save = function () {
        var _this = this;
        this.isSaving = true;
        if (this.produitCommande.id) {
            this.produitCommandeService()
                .update(this.produitCommande)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.produitCommande.updated', { param: param.id });
                return _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Info',
                    variant: 'info',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
        else {
            this.produitCommandeService()
                .create(this.produitCommande)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.produitCommande.created', { param: param.id });
                _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Success',
                    variant: 'success',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
    };
    ProduitCommandeUpdate.prototype.retrieveProduitCommande = function (produitCommandeId) {
        var _this = this;
        this.produitCommandeService()
            .find(produitCommandeId)
            .then(function (res) {
            _this.produitCommande = res;
        })
            .catch(function (error) {
            _this.alertService().showHttpError(_this, error.response);
        });
    };
    ProduitCommandeUpdate.prototype.previousState = function () {
        this.$router.go(-1);
    };
    ProduitCommandeUpdate.prototype.initRelationships = function () {
        var _this = this;
        this.produitService()
            .retrieve()
            .then(function (res) {
            _this.produits = res.data;
        });
        this.commandeService()
            .retrieve()
            .then(function (res) {
            _this.commandes = res.data;
        });
    };
    ProduitCommandeUpdate.prototype.getSelected = function (selectedVals, option) {
        var _a;
        if (selectedVals) {
            return (_a = selectedVals.find(function (value) { return option.id === value.id; })) !== null && _a !== void 0 ? _a : option;
        }
        return option;
    };
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('produitCommandeService'),
        __metadata("design:type", Function)
    ], ProduitCommandeUpdate.prototype, "produitCommandeService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('alertService'),
        __metadata("design:type", Function)
    ], ProduitCommandeUpdate.prototype, "alertService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('produitService'),
        __metadata("design:type", Function)
    ], ProduitCommandeUpdate.prototype, "produitService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('commandeService'),
        __metadata("design:type", Function)
    ], ProduitCommandeUpdate.prototype, "commandeService", void 0);
    ProduitCommandeUpdate = __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Component)({
            validations: validations,
        })
    ], ProduitCommandeUpdate);
    return ProduitCommandeUpdate;
}(vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Vue));
/* harmony default export */ __webpack_exports__["default"] = (ProduitCommandeUpdate);


/***/ }),

/***/ "./src/main/webapp/app/shared/model/produit-commande.model.ts":
/*!********************************************************************!*\
  !*** ./src/main/webapp/app/shared/model/produit-commande.model.ts ***!
  \********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProduitCommande": function() { return /* binding */ ProduitCommande; }
/* harmony export */ });
var ProduitCommande = /** @class */ (function () {
    function ProduitCommande(id, idProduit, idCommande, quantite, quantitePossible, products, commande) {
        var _a;
        this.id = id;
        this.idProduit = idProduit;
        this.idCommande = idCommande;
        this.quantite = quantite;
        this.quantitePossible = quantitePossible;
        this.products = products;
        this.commande = commande;
        this.quantitePossible = (_a = this.quantitePossible) !== null && _a !== void 0 ? _a : false;
    }
    return ProduitCommande;
}());



/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue":
/*!***********************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue ***!
  \***********************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./produit-commande-update.vue?vue&type=template&id=a87f765c& */ "./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c&");
/* harmony import */ var _produit_commande_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./produit-commande-update.component.ts?vue&type=script&lang=ts& */ "./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _produit_commande_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__.render,
  _produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/main/webapp/app/entities/produit-commande/produit-commande-update.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts&":
/*!*********************************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts& ***!
  \*********************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_commande_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./produit-commande-update.component.ts?vue&type=script&lang=ts& */ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande-update.component.ts?vue&type=script&lang=ts&");
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_commande_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c&":
/*!******************************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c& ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__.render; },
/* harmony export */   "staticRenderFns": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns; }
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_update_vue_vue_type_template_id_a87f765c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./produit-commande-update.vue?vue&type=template&id=a87f765c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande-update.vue?vue&type=template&id=a87f765c& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; },
/* harmony export */   "staticRenderFns": function() { return /* binding */ staticRenderFns; }
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c,
    _setup = _vm._self._setupProxy
  return _c("div", { staticClass: "row justify-content-center" }, [
    _c("div", { staticClass: "col-8" }, [
      _c(
        "form",
        {
          attrs: { name: "editForm", role: "form", novalidate: "" },
          on: {
            submit: function ($event) {
              $event.preventDefault()
              return _vm.save()
            },
          },
        },
        [
          _c(
            "h2",
            {
              attrs: {
                id: "blogApp.produitCommande.home.createOrEditLabel",
                "data-cy": "ProduitCommandeCreateUpdateHeading",
              },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.produitCommande.home.createOrEditLabel")
                ),
              },
            },
            [_vm._v("\n        Create or edit a ProduitCommande\n      ")]
          ),
          _vm._v(" "),
          _c("div", [
            _vm.produitCommande.id
              ? _c("div", { staticClass: "form-group" }, [
                  _c(
                    "label",
                    {
                      attrs: { for: "id" },
                      domProps: {
                        textContent: _vm._s(_vm.$t("global.field.id")),
                      },
                    },
                    [_vm._v("ID")]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.produitCommande.id,
                        expression: "produitCommande.id",
                      },
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "id", name: "id", readonly: "" },
                    domProps: { value: _vm.produitCommande.id },
                    on: {
                      input: function ($event) {
                        if ($event.target.composing) return
                        _vm.$set(_vm.produitCommande, "id", $event.target.value)
                      },
                    },
                  }),
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-commande-idProduit" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.idProduit")
                    ),
                  },
                },
                [_vm._v("Id Produit")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produitCommande.idProduit.$model,
                    expression: "$v.produitCommande.idProduit.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produitCommande.idProduit.$invalid,
                  invalid: _vm.$v.produitCommande.idProduit.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "idProduit",
                  id: "produit-commande-idProduit",
                  "data-cy": "idProduit",
                  required: "",
                },
                domProps: { value: _vm.$v.produitCommande.idProduit.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produitCommande.idProduit,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produitCommande.idProduit.$anyDirty &&
              _vm.$v.produitCommande.idProduit.$invalid
                ? _c("div", [
                    !_vm.$v.produitCommande.idProduit.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produitCommande.idProduit.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-commande-idCommande" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.idCommande")
                    ),
                  },
                },
                [_vm._v("Id Commande")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produitCommande.idCommande.$model,
                    expression: "$v.produitCommande.idCommande.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produitCommande.idCommande.$invalid,
                  invalid: _vm.$v.produitCommande.idCommande.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "idCommande",
                  id: "produit-commande-idCommande",
                  "data-cy": "idCommande",
                  required: "",
                },
                domProps: { value: _vm.$v.produitCommande.idCommande.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produitCommande.idCommande,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produitCommande.idCommande.$anyDirty &&
              _vm.$v.produitCommande.idCommande.$invalid
                ? _c("div", [
                    !_vm.$v.produitCommande.idCommande.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produitCommande.idCommande.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-commande-quantite" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.quantite")
                    ),
                  },
                },
                [_vm._v("Quantite")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produitCommande.quantite.$model,
                    expression: "$v.produitCommande.quantite.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produitCommande.quantite.$invalid,
                  invalid: _vm.$v.produitCommande.quantite.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "quantite",
                  id: "produit-commande-quantite",
                  "data-cy": "quantite",
                },
                domProps: { value: _vm.$v.produitCommande.quantite.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produitCommande.quantite,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-commande-quantitePossible" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.quantitePossible")
                    ),
                  },
                },
                [_vm._v("Quantite Possible")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.$v.produitCommande.quantitePossible.$model,
                    expression: "$v.produitCommande.quantitePossible.$model",
                  },
                ],
                staticClass: "form-check",
                class: {
                  valid: !_vm.$v.produitCommande.quantitePossible.$invalid,
                  invalid: _vm.$v.produitCommande.quantitePossible.$invalid,
                },
                attrs: {
                  type: "checkbox",
                  name: "quantitePossible",
                  id: "produit-commande-quantitePossible",
                  "data-cy": "quantitePossible",
                },
                domProps: {
                  checked: Array.isArray(
                    _vm.$v.produitCommande.quantitePossible.$model
                  )
                    ? _vm._i(
                        _vm.$v.produitCommande.quantitePossible.$model,
                        null
                      ) > -1
                    : _vm.$v.produitCommande.quantitePossible.$model,
                },
                on: {
                  change: function ($event) {
                    var $$a = _vm.$v.produitCommande.quantitePossible.$model,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = null,
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(
                            _vm.$v.produitCommande.quantitePossible,
                            "$model",
                            $$a.concat([$$v])
                          )
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.$v.produitCommande.quantitePossible,
                            "$model",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(
                        _vm.$v.produitCommande.quantitePossible,
                        "$model",
                        $$c
                      )
                    }
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  attrs: { for: "produit-commande-product" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.product")
                    ),
                  },
                },
                [_vm._v("Product")]
              ),
              _vm._v(" "),
              _vm.produitCommande.products !== undefined
                ? _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.produitCommande.products,
                          expression: "produitCommande.products",
                        },
                      ],
                      staticClass: "form-control",
                      attrs: {
                        id: "produit-commande-products",
                        "data-cy": "product",
                        multiple: "",
                        name: "product",
                        required: "",
                      },
                      on: {
                        change: function ($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function (o) {
                              return o.selected
                            })
                            .map(function (o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.produitCommande,
                            "products",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        },
                      },
                    },
                    _vm._l(_vm.produits, function (produitOption) {
                      return _c(
                        "option",
                        {
                          key: produitOption.id,
                          domProps: {
                            value: _vm.getSelected(
                              _vm.produitCommande.products,
                              produitOption
                            ),
                          },
                        },
                        [
                          _vm._v(
                            "\n              " +
                              _vm._s(produitOption.id) +
                              "\n            "
                          ),
                        ]
                      )
                    }),
                    0
                  )
                : _vm._e(),
            ]),
            _vm._v(" "),
            _vm.$v.produitCommande.products.$anyDirty &&
            _vm.$v.produitCommande.products.$invalid
              ? _c("div", [
                  !_vm.$v.produitCommande.products.required
                    ? _c(
                        "small",
                        {
                          staticClass: "form-text text-danger",
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("entity.validation.required")
                            ),
                          },
                        },
                        [
                          _vm._v(
                            "\n            This field is required.\n          "
                          ),
                        ]
                      )
                    : _vm._e(),
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-commande-commande" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produitCommande.commande")
                    ),
                  },
                },
                [_vm._v("Commande")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.produitCommande.commande,
                      expression: "produitCommande.commande",
                    },
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "produit-commande-commande",
                    "data-cy": "commande",
                    name: "commande",
                  },
                  on: {
                    change: function ($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function (o) {
                          return o.selected
                        })
                        .map(function (o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.produitCommande,
                        "commande",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    },
                  },
                },
                [
                  _c("option", { domProps: { value: null } }),
                  _vm._v(" "),
                  _vm._l(_vm.commandes, function (commandeOption) {
                    return _c(
                      "option",
                      {
                        key: commandeOption.id,
                        domProps: {
                          value:
                            _vm.produitCommande.commande &&
                            commandeOption.id ===
                              _vm.produitCommande.commande.id
                              ? _vm.produitCommande.commande
                              : commandeOption,
                        },
                      },
                      [
                        _vm._v(
                          "\n              " +
                            _vm._s(commandeOption.id) +
                            "\n            "
                        ),
                      ]
                    )
                  }),
                ],
                2
              ),
            ]),
          ]),
          _vm._v(" "),
          _c("div", [
            _c(
              "button",
              {
                staticClass: "btn btn-secondary",
                attrs: {
                  type: "button",
                  id: "cancel-save",
                  "data-cy": "entityCreateCancelButton",
                },
                on: {
                  click: function ($event) {
                    return _vm.previousState()
                  },
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "ban" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.cancel")),
                    },
                  },
                  [_vm._v("Cancel")]
                ),
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: {
                  type: "submit",
                  id: "save-entity",
                  "data-cy": "entityCreateSaveButton",
                  disabled: _vm.$v.produitCommande.$invalid || _vm.isSaving,
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "save" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.save")),
                    },
                  },
                  [_vm._v("Save")]
                ),
              ],
              1
            ),
          ]),
        ]
      ),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_produit-commande_produit-commande-update_vue.d9747c7c103bf802e2e7.chunk.js.map