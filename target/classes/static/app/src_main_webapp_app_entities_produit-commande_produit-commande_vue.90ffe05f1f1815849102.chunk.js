"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_produit-commande_produit-commande_vue"],{

/***/ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts& ***!
  \************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-property-decorator */ "./node_modules/vue-property-decorator/lib/index.js");
/* harmony import */ var vue2_filters__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-filters */ "./node_modules/vue2-filters/dist/vue2-filters.js");
/* harmony import */ var vue2_filters__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_filters__WEBPACK_IMPORTED_MODULE_1__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProduitCommande = /** @class */ (function (_super) {
    __extends(ProduitCommande, _super);
    function ProduitCommande() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.removeId = null;
        _this.produitCommandes = [];
        _this.isFetching = false;
        return _this;
    }
    ProduitCommande.prototype.mounted = function () {
        this.retrieveAllProduitCommandes();
    };
    ProduitCommande.prototype.clear = function () {
        this.retrieveAllProduitCommandes();
    };
    ProduitCommande.prototype.retrieveAllProduitCommandes = function () {
        var _this = this;
        this.isFetching = true;
        this.produitCommandeService()
            .retrieve()
            .then(function (res) {
            _this.produitCommandes = res.data;
            _this.isFetching = false;
        }, function (err) {
            _this.isFetching = false;
            _this.alertService().showHttpError(_this, err.response);
        });
    };
    ProduitCommande.prototype.handleSyncList = function () {
        this.clear();
    };
    ProduitCommande.prototype.prepareRemove = function (instance) {
        this.removeId = instance.id;
        if (this.$refs.removeEntity) {
            this.$refs.removeEntity.show();
        }
    };
    ProduitCommande.prototype.removeProduitCommande = function () {
        var _this = this;
        this.produitCommandeService()
            .delete(this.removeId)
            .then(function () {
            var message = _this.$t('blogApp.produitCommande.deleted', { param: _this.removeId });
            _this.$bvToast.toast(message.toString(), {
                toaster: 'b-toaster-top-center',
                title: 'Info',
                variant: 'danger',
                solid: true,
                autoHideDelay: 5000,
            });
            _this.removeId = null;
            _this.retrieveAllProduitCommandes();
            _this.closeDialog();
        })
            .catch(function (error) {
            _this.alertService().showHttpError(_this, error.response);
        });
    };
    ProduitCommande.prototype.closeDialog = function () {
        this.$refs.removeEntity.hide();
    };
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('produitCommandeService'),
        __metadata("design:type", Function)
    ], ProduitCommande.prototype, "produitCommandeService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('alertService'),
        __metadata("design:type", Function)
    ], ProduitCommande.prototype, "alertService", void 0);
    ProduitCommande = __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Component)({
            mixins: [(vue2_filters__WEBPACK_IMPORTED_MODULE_1___default().mixin)],
        })
    ], ProduitCommande);
    return ProduitCommande;
}(vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Vue));
/* harmony default export */ __webpack_exports__["default"] = (ProduitCommande);


/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande.vue":
/*!****************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande.vue ***!
  \****************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./produit-commande.vue?vue&type=template&id=71fe95d8& */ "./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8&");
/* harmony import */ var _produit_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./produit-commande.component.ts?vue&type=script&lang=ts& */ "./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _produit_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__.render,
  _produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/main/webapp/app/entities/produit-commande/produit-commande.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts&":
/*!**************************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts& ***!
  \**************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./produit-commande.component.ts?vue&type=script&lang=ts& */ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit-commande/produit-commande.component.ts?vue&type=script&lang=ts&");
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_commande_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8&":
/*!***********************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8& ***!
  \***********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__.render; },
/* harmony export */   "staticRenderFns": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns; }
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_commande_vue_vue_type_template_id_71fe95d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./produit-commande.vue?vue&type=template&id=71fe95d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit-commande/produit-commande.vue?vue&type=template&id=71fe95d8& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; },
/* harmony export */   "staticRenderFns": function() { return /* binding */ staticRenderFns; }
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c,
    _setup = _vm._self._setupProxy
  return _c(
    "div",
    [
      _c(
        "h2",
        { attrs: { id: "page-heading", "data-cy": "ProduitCommandeHeading" } },
        [
          _c(
            "span",
            {
              attrs: { id: "produit-commande-heading" },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.produitCommande.home.title")
                ),
              },
            },
            [_vm._v("Produit Commandes")]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-flex justify-content-end" },
            [
              _c(
                "button",
                {
                  staticClass: "btn btn-info mr-2",
                  attrs: { disabled: _vm.isFetching },
                  on: { click: _vm.handleSyncList },
                },
                [
                  _c("font-awesome-icon", {
                    attrs: { icon: "sync", spin: _vm.isFetching },
                  }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      domProps: {
                        textContent: _vm._s(
                          _vm.$t(
                            "blogApp.produitCommande.home.refreshListLabel"
                          )
                        ),
                      },
                    },
                    [_vm._v("Refresh List")]
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c("router-link", {
                attrs: { to: { name: "ProduitCommandeCreate" }, custom: "" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function ({ navigate }) {
                      return [
                        _c(
                          "button",
                          {
                            staticClass:
                              "btn btn-primary jh-create-entity create-produit-commande",
                            attrs: {
                              id: "jh-create-entity",
                              "data-cy": "entityCreateButton",
                            },
                            on: { click: navigate },
                          },
                          [
                            _c("font-awesome-icon", {
                              attrs: { icon: "plus" },
                            }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                domProps: {
                                  textContent: _vm._s(
                                    _vm.$t(
                                      "blogApp.produitCommande.home.createLabel"
                                    )
                                  ),
                                },
                              },
                              [_vm._v(" Create a new Produit Commande ")]
                            ),
                          ],
                          1
                        ),
                      ]
                    },
                  },
                ]),
              }),
            ],
            1
          ),
        ]
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      !_vm.isFetching &&
      _vm.produitCommandes &&
      _vm.produitCommandes.length === 0
        ? _c("div", { staticClass: "alert alert-warning" }, [
            _c(
              "span",
              {
                domProps: {
                  textContent: _vm._s(
                    _vm.$t("blogApp.produitCommande.home.notFound")
                  ),
                },
              },
              [_vm._v("No produitCommandes found")]
            ),
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.produitCommandes && _vm.produitCommandes.length > 0
        ? _c("div", { staticClass: "table-responsive" }, [
            _c(
              "table",
              {
                staticClass: "table table-striped",
                attrs: { "aria-describedby": "produitCommandes" },
              },
              [
                _c("thead", [
                  _c("tr", [
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(_vm.$t("global.field.id")),
                          },
                        },
                        [_vm._v("ID")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.idProduit")
                            ),
                          },
                        },
                        [_vm._v("Id Produit")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.idCommande")
                            ),
                          },
                        },
                        [_vm._v("Id Commande")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.quantite")
                            ),
                          },
                        },
                        [_vm._v("Quantite")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.quantitePossible")
                            ),
                          },
                        },
                        [_vm._v("Quantite Possible")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.product")
                            ),
                          },
                        },
                        [_vm._v("Product")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }, [
                      _c(
                        "span",
                        {
                          domProps: {
                            textContent: _vm._s(
                              _vm.$t("blogApp.produitCommande.commande")
                            ),
                          },
                        },
                        [_vm._v("Commande")]
                      ),
                    ]),
                    _vm._v(" "),
                    _c("th", { attrs: { scope: "row" } }),
                  ]),
                ]),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.produitCommandes, function (produitCommande) {
                    return _c(
                      "tr",
                      {
                        key: produitCommande.id,
                        attrs: { "data-cy": "entityTable" },
                      },
                      [
                        _c(
                          "td",
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "ProduitCommandeView",
                                    params: {
                                      produitCommandeId: produitCommande.id,
                                    },
                                  },
                                },
                              },
                              [_vm._v(_vm._s(produitCommande.id))]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(produitCommande.idProduit))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(produitCommande.idCommande))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(produitCommande.quantite))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(_vm._s(produitCommande.quantitePossible)),
                        ]),
                        _vm._v(" "),
                        _c(
                          "td",
                          _vm._l(
                            produitCommande.products,
                            function (product, i) {
                              return _c(
                                "span",
                                { key: product.id },
                                [
                                  _vm._v(
                                    _vm._s(i > 0 ? ", " : "") +
                                      "\n              "
                                  ),
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "form-control-static",
                                      attrs: {
                                        to: {
                                          name: "ProduitView",
                                          params: { produitId: product.id },
                                        },
                                      },
                                    },
                                    [_vm._v(_vm._s(product.id))]
                                  ),
                                ],
                                1
                              )
                            }
                          ),
                          0
                        ),
                        _vm._v(" "),
                        _c("td", [
                          produitCommande.commande
                            ? _c(
                                "div",
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "CommandeView",
                                          params: {
                                            commandeId:
                                              produitCommande.commande.id,
                                          },
                                        },
                                      },
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(produitCommande.commande.id)
                                      ),
                                    ]
                                  ),
                                ],
                                1
                              )
                            : _vm._e(),
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "text-right" }, [
                          _c(
                            "div",
                            { staticClass: "btn-group" },
                            [
                              _c("router-link", {
                                attrs: {
                                  to: {
                                    name: "ProduitCommandeView",
                                    params: {
                                      produitCommandeId: produitCommande.id,
                                    },
                                  },
                                  custom: "",
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "default",
                                      fn: function ({ navigate }) {
                                        return [
                                          _c(
                                            "button",
                                            {
                                              staticClass:
                                                "btn btn-info btn-sm details",
                                              attrs: {
                                                "data-cy":
                                                  "entityDetailsButton",
                                              },
                                              on: { click: navigate },
                                            },
                                            [
                                              _c("font-awesome-icon", {
                                                attrs: { icon: "eye" },
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "d-none d-md-inline",
                                                  domProps: {
                                                    textContent: _vm._s(
                                                      _vm.$t(
                                                        "entity.action.view"
                                                      )
                                                    ),
                                                  },
                                                },
                                                [_vm._v("View")]
                                              ),
                                            ],
                                            1
                                          ),
                                        ]
                                      },
                                    },
                                  ],
                                  null,
                                  true
                                ),
                              }),
                              _vm._v(" "),
                              _c("router-link", {
                                attrs: {
                                  to: {
                                    name: "ProduitCommandeEdit",
                                    params: {
                                      produitCommandeId: produitCommande.id,
                                    },
                                  },
                                  custom: "",
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "default",
                                      fn: function ({ navigate }) {
                                        return [
                                          _c(
                                            "button",
                                            {
                                              staticClass:
                                                "btn btn-primary btn-sm edit",
                                              attrs: {
                                                "data-cy": "entityEditButton",
                                              },
                                              on: { click: navigate },
                                            },
                                            [
                                              _c("font-awesome-icon", {
                                                attrs: { icon: "pencil-alt" },
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "d-none d-md-inline",
                                                  domProps: {
                                                    textContent: _vm._s(
                                                      _vm.$t(
                                                        "entity.action.edit"
                                                      )
                                                    ),
                                                  },
                                                },
                                                [_vm._v("Edit")]
                                              ),
                                            ],
                                            1
                                          ),
                                        ]
                                      },
                                    },
                                  ],
                                  null,
                                  true
                                ),
                              }),
                              _vm._v(" "),
                              _c(
                                "b-button",
                                {
                                  directives: [
                                    {
                                      name: "b-modal",
                                      rawName: "v-b-modal.removeEntity",
                                      modifiers: { removeEntity: true },
                                    },
                                  ],
                                  staticClass: "btn btn-sm",
                                  attrs: {
                                    variant: "danger",
                                    "data-cy": "entityDeleteButton",
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.prepareRemove(produitCommande)
                                    },
                                  },
                                },
                                [
                                  _c("font-awesome-icon", {
                                    attrs: { icon: "times" },
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass: "d-none d-md-inline",
                                      domProps: {
                                        textContent: _vm._s(
                                          _vm.$t("entity.action.delete")
                                        ),
                                      },
                                    },
                                    [_vm._v("Delete")]
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ]),
                      ]
                    )
                  }),
                  0
                ),
              ]
            ),
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("b-modal", { ref: "removeEntity", attrs: { id: "removeEntity" } }, [
        _c("span", { attrs: { slot: "modal-title" }, slot: "modal-title" }, [
          _c(
            "span",
            {
              attrs: {
                id: "blogApp.produitCommande.delete.question",
                "data-cy": "produitCommandeDeleteDialogHeading",
              },
              domProps: { textContent: _vm._s(_vm.$t("entity.delete.title")) },
            },
            [_vm._v("Confirm delete operation")]
          ),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "modal-body" }, [
          _c(
            "p",
            {
              attrs: { id: "jhi-delete-produitCommande-heading" },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.produitCommande.delete.question", {
                    id: _vm.removeId,
                  })
                ),
              },
            },
            [
              _vm._v(
                "\n        Are you sure you want to delete this Produit Commande?\n      "
              ),
            ]
          ),
        ]),
        _vm._v(" "),
        _c("div", { attrs: { slot: "modal-footer" }, slot: "modal-footer" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-secondary",
              attrs: { type: "button" },
              domProps: { textContent: _vm._s(_vm.$t("entity.action.cancel")) },
              on: {
                click: function ($event) {
                  return _vm.closeDialog()
                },
              },
            },
            [_vm._v("Cancel")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-primary",
              attrs: {
                type: "button",
                id: "jhi-confirm-delete-produitCommande",
                "data-cy": "entityConfirmDeleteButton",
              },
              domProps: { textContent: _vm._s(_vm.$t("entity.action.delete")) },
              on: {
                click: function ($event) {
                  return _vm.removeProduitCommande()
                },
              },
            },
            [_vm._v("\n        Delete\n      ")]
          ),
        ]),
      ]),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_produit-commande_produit-commande_vue.90ffe05f1f1815849102.chunk.js.map