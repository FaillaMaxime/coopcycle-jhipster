"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_produit_produit-update_vue"],{

/***/ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts& ***!
  \*************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-property-decorator */ "./node_modules/vue-property-decorator/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var _shared_model_produit_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/shared/model/produit.model */ "./src/main/webapp/app/shared/model/produit.model.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var validations = {
    produit: {
        idProduit: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
            numeric: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.numeric,
        },
        idMenu: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
            numeric: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.numeric,
        },
        nom: {},
        prix: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required,
            decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.decimal,
        },
        disponibilite: {
            numeric: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.numeric,
            min: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.minValue)(0),
        },
    },
};
var ProduitUpdate = /** @class */ (function (_super) {
    __extends(ProduitUpdate, _super);
    function ProduitUpdate() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.produit = new _shared_model_produit_model__WEBPACK_IMPORTED_MODULE_1__.Produit();
        _this.menus = [];
        _this.produitCommandes = [];
        _this.isSaving = false;
        _this.currentLanguage = '';
        return _this;
    }
    ProduitUpdate.prototype.beforeRouteEnter = function (to, from, next) {
        next(function (vm) {
            if (to.params.produitId) {
                vm.retrieveProduit(to.params.produitId);
            }
            vm.initRelationships();
        });
    };
    ProduitUpdate.prototype.created = function () {
        var _this = this;
        this.currentLanguage = this.$store.getters.currentLanguage;
        this.$store.watch(function () { return _this.$store.getters.currentLanguage; }, function () {
            _this.currentLanguage = _this.$store.getters.currentLanguage;
        });
    };
    ProduitUpdate.prototype.save = function () {
        var _this = this;
        this.isSaving = true;
        if (this.produit.id) {
            this.produitService()
                .update(this.produit)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.produit.updated', { param: param.id });
                return _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Info',
                    variant: 'info',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
        else {
            this.produitService()
                .create(this.produit)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.produit.created', { param: param.id });
                _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Success',
                    variant: 'success',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
    };
    ProduitUpdate.prototype.retrieveProduit = function (produitId) {
        var _this = this;
        this.produitService()
            .find(produitId)
            .then(function (res) {
            _this.produit = res;
        })
            .catch(function (error) {
            _this.alertService().showHttpError(_this, error.response);
        });
    };
    ProduitUpdate.prototype.previousState = function () {
        this.$router.go(-1);
    };
    ProduitUpdate.prototype.initRelationships = function () {
        var _this = this;
        this.menuService()
            .retrieve()
            .then(function (res) {
            _this.menus = res.data;
        });
        this.produitCommandeService()
            .retrieve()
            .then(function (res) {
            _this.produitCommandes = res.data;
        });
    };
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('produitService'),
        __metadata("design:type", Function)
    ], ProduitUpdate.prototype, "produitService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('alertService'),
        __metadata("design:type", Function)
    ], ProduitUpdate.prototype, "alertService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('menuService'),
        __metadata("design:type", Function)
    ], ProduitUpdate.prototype, "menuService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('produitCommandeService'),
        __metadata("design:type", Function)
    ], ProduitUpdate.prototype, "produitCommandeService", void 0);
    ProduitUpdate = __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Component)({
            validations: validations,
        })
    ], ProduitUpdate);
    return ProduitUpdate;
}(vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Vue));
/* harmony default export */ __webpack_exports__["default"] = (ProduitUpdate);


/***/ }),

/***/ "./src/main/webapp/app/shared/model/produit.model.ts":
/*!***********************************************************!*\
  !*** ./src/main/webapp/app/shared/model/produit.model.ts ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Produit": function() { return /* binding */ Produit; }
/* harmony export */ });
var Produit = /** @class */ (function () {
    function Produit(id, idProduit, idMenu, nom, prix, disponibilite, menu, produitcommandes) {
        this.id = id;
        this.idProduit = idProduit;
        this.idMenu = idMenu;
        this.nom = nom;
        this.prix = prix;
        this.disponibilite = disponibilite;
        this.menu = menu;
        this.produitcommandes = produitcommandes;
    }
    return Produit;
}());



/***/ }),

/***/ "./src/main/webapp/app/entities/produit/produit-update.vue":
/*!*****************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit/produit-update.vue ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./produit-update.vue?vue&type=template&id=6d7589be& */ "./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be&");
/* harmony import */ var _produit_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./produit-update.component.ts?vue&type=script&lang=ts& */ "./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _produit_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__.render,
  _produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/main/webapp/app/entities/produit/produit-update.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts&":
/*!***************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts& ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./produit-update.component.ts?vue&type=script&lang=ts& */ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/produit/produit-update.component.ts?vue&type=script&lang=ts&");
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_produit_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be&":
/*!************************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be& ***!
  \************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__.render; },
/* harmony export */   "staticRenderFns": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns; }
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_produit_update_vue_vue_type_template_id_6d7589be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./produit-update.vue?vue&type=template&id=6d7589be& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/produit/produit-update.vue?vue&type=template&id=6d7589be& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; },
/* harmony export */   "staticRenderFns": function() { return /* binding */ staticRenderFns; }
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c,
    _setup = _vm._self._setupProxy
  return _c("div", { staticClass: "row justify-content-center" }, [
    _c("div", { staticClass: "col-8" }, [
      _c(
        "form",
        {
          attrs: { name: "editForm", role: "form", novalidate: "" },
          on: {
            submit: function ($event) {
              $event.preventDefault()
              return _vm.save()
            },
          },
        },
        [
          _c(
            "h2",
            {
              attrs: {
                id: "blogApp.produit.home.createOrEditLabel",
                "data-cy": "ProduitCreateUpdateHeading",
              },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.produit.home.createOrEditLabel")
                ),
              },
            },
            [_vm._v("\n        Create or edit a Produit\n      ")]
          ),
          _vm._v(" "),
          _c("div", [
            _vm.produit.id
              ? _c("div", { staticClass: "form-group" }, [
                  _c(
                    "label",
                    {
                      attrs: { for: "id" },
                      domProps: {
                        textContent: _vm._s(_vm.$t("global.field.id")),
                      },
                    },
                    [_vm._v("ID")]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.produit.id,
                        expression: "produit.id",
                      },
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "id", name: "id", readonly: "" },
                    domProps: { value: _vm.produit.id },
                    on: {
                      input: function ($event) {
                        if ($event.target.composing) return
                        _vm.$set(_vm.produit, "id", $event.target.value)
                      },
                    },
                  }),
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-idProduit" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.produit.idProduit")),
                  },
                },
                [_vm._v("Id Produit")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produit.idProduit.$model,
                    expression: "$v.produit.idProduit.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produit.idProduit.$invalid,
                  invalid: _vm.$v.produit.idProduit.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "idProduit",
                  id: "produit-idProduit",
                  "data-cy": "idProduit",
                  required: "",
                },
                domProps: { value: _vm.$v.produit.idProduit.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produit.idProduit,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produit.idProduit.$anyDirty &&
              _vm.$v.produit.idProduit.$invalid
                ? _c("div", [
                    !_vm.$v.produit.idProduit.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produit.idProduit.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-idMenu" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.produit.idMenu")),
                  },
                },
                [_vm._v("Id Menu")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produit.idMenu.$model,
                    expression: "$v.produit.idMenu.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produit.idMenu.$invalid,
                  invalid: _vm.$v.produit.idMenu.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "idMenu",
                  id: "produit-idMenu",
                  "data-cy": "idMenu",
                  required: "",
                },
                domProps: { value: _vm.$v.produit.idMenu.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produit.idMenu,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produit.idMenu.$anyDirty && _vm.$v.produit.idMenu.$invalid
                ? _c("div", [
                    !_vm.$v.produit.idMenu.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produit.idMenu.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-nom" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.produit.nom")),
                  },
                },
                [_vm._v("Nom")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.$v.produit.nom.$model,
                    expression: "$v.produit.nom.$model",
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produit.nom.$invalid,
                  invalid: _vm.$v.produit.nom.$invalid,
                },
                attrs: {
                  type: "text",
                  name: "nom",
                  id: "produit-nom",
                  "data-cy": "nom",
                },
                domProps: { value: _vm.$v.produit.nom.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(_vm.$v.produit.nom, "$model", $event.target.value)
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-prix" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.produit.prix")),
                  },
                },
                [_vm._v("Prix")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produit.prix.$model,
                    expression: "$v.produit.prix.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produit.prix.$invalid,
                  invalid: _vm.$v.produit.prix.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "prix",
                  id: "produit-prix",
                  "data-cy": "prix",
                  required: "",
                },
                domProps: { value: _vm.$v.produit.prix.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produit.prix,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produit.prix.$anyDirty && _vm.$v.produit.prix.$invalid
                ? _c("div", [
                    !_vm.$v.produit.prix.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produit.prix.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-disponibilite" },
                  domProps: {
                    textContent: _vm._s(
                      _vm.$t("blogApp.produit.disponibilite")
                    ),
                  },
                },
                [_vm._v("Disponibilite")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.$v.produit.disponibilite.$model,
                    expression: "$v.produit.disponibilite.$model",
                    modifiers: { number: true },
                  },
                ],
                staticClass: "form-control",
                class: {
                  valid: !_vm.$v.produit.disponibilite.$invalid,
                  invalid: _vm.$v.produit.disponibilite.$invalid,
                },
                attrs: {
                  type: "number",
                  name: "disponibilite",
                  id: "produit-disponibilite",
                  "data-cy": "disponibilite",
                },
                domProps: { value: _vm.$v.produit.disponibilite.$model },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) return
                    _vm.$set(
                      _vm.$v.produit.disponibilite,
                      "$model",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function ($event) {
                    return _vm.$forceUpdate()
                  },
                },
              }),
              _vm._v(" "),
              _vm.$v.produit.disponibilite.$anyDirty &&
              _vm.$v.produit.disponibilite.$invalid
                ? _c("div", [
                    !_vm.$v.produit.disponibilite.min
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.min", { min: 0 })
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be at least 0.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.$v.produit.disponibilite.numeric
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.number")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field should be a number.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "produit-menu" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.produit.menu")),
                  },
                },
                [_vm._v("Menu")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.produit.menu,
                      expression: "produit.menu",
                    },
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "produit-menu",
                    "data-cy": "menu",
                    name: "menu",
                  },
                  on: {
                    change: function ($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function (o) {
                          return o.selected
                        })
                        .map(function (o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.produit,
                        "menu",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    },
                  },
                },
                [
                  _c("option", { domProps: { value: null } }),
                  _vm._v(" "),
                  _vm._l(_vm.menus, function (menuOption) {
                    return _c(
                      "option",
                      {
                        key: menuOption.id,
                        domProps: {
                          value:
                            _vm.produit.menu &&
                            menuOption.id === _vm.produit.menu.id
                              ? _vm.produit.menu
                              : menuOption,
                        },
                      },
                      [
                        _vm._v(
                          "\n              " +
                            _vm._s(menuOption.id) +
                            "\n            "
                        ),
                      ]
                    )
                  }),
                ],
                2
              ),
            ]),
          ]),
          _vm._v(" "),
          _c("div", [
            _c(
              "button",
              {
                staticClass: "btn btn-secondary",
                attrs: {
                  type: "button",
                  id: "cancel-save",
                  "data-cy": "entityCreateCancelButton",
                },
                on: {
                  click: function ($event) {
                    return _vm.previousState()
                  },
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "ban" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.cancel")),
                    },
                  },
                  [_vm._v("Cancel")]
                ),
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: {
                  type: "submit",
                  id: "save-entity",
                  "data-cy": "entityCreateSaveButton",
                  disabled: _vm.$v.produit.$invalid || _vm.isSaving,
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "save" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.save")),
                    },
                  },
                  [_vm._v("Save")]
                ),
              ],
              1
            ),
          ]),
        ]
      ),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_produit_produit-update_vue.7cee1868d9cbcc4455b1.chunk.js.map