"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_role_role-update_vue"],{

/***/ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts&":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts& ***!
  \*******************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-property-decorator */ "./node_modules/vue-property-decorator/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var _shared_model_role_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/shared/model/role.model */ "./src/main/webapp/app/shared/model/role.model.ts");
/* harmony import */ var _shared_model_enumerations_role_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/shared/model/enumerations/role.model */ "./src/main/webapp/app/shared/model/enumerations/role.model.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var validations = {
    role: {
        role: {
            required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_3__.required,
        },
    },
};
var RoleUpdate = /** @class */ (function (_super) {
    __extends(RoleUpdate, _super);
    function RoleUpdate() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.role = new _shared_model_role_model__WEBPACK_IMPORTED_MODULE_1__.Role();
        _this.utilisateurs = [];
        _this.rOLEValues = Object.keys(_shared_model_enumerations_role_model__WEBPACK_IMPORTED_MODULE_2__.ROLE);
        _this.isSaving = false;
        _this.currentLanguage = '';
        return _this;
    }
    RoleUpdate.prototype.beforeRouteEnter = function (to, from, next) {
        next(function (vm) {
            if (to.params.roleId) {
                vm.retrieveRole(to.params.roleId);
            }
            vm.initRelationships();
        });
    };
    RoleUpdate.prototype.created = function () {
        var _this = this;
        this.currentLanguage = this.$store.getters.currentLanguage;
        this.$store.watch(function () { return _this.$store.getters.currentLanguage; }, function () {
            _this.currentLanguage = _this.$store.getters.currentLanguage;
        });
    };
    RoleUpdate.prototype.save = function () {
        var _this = this;
        this.isSaving = true;
        if (this.role.id) {
            this.roleService()
                .update(this.role)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.role.updated', { param: param.id });
                return _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Info',
                    variant: 'info',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
        else {
            this.roleService()
                .create(this.role)
                .then(function (param) {
                _this.isSaving = false;
                _this.$router.go(-1);
                var message = _this.$t('blogApp.role.created', { param: param.id });
                _this.$root.$bvToast.toast(message.toString(), {
                    toaster: 'b-toaster-top-center',
                    title: 'Success',
                    variant: 'success',
                    solid: true,
                    autoHideDelay: 5000,
                });
            })
                .catch(function (error) {
                _this.isSaving = false;
                _this.alertService().showHttpError(_this, error.response);
            });
        }
    };
    RoleUpdate.prototype.retrieveRole = function (roleId) {
        var _this = this;
        this.roleService()
            .find(roleId)
            .then(function (res) {
            _this.role = res;
        })
            .catch(function (error) {
            _this.alertService().showHttpError(_this, error.response);
        });
    };
    RoleUpdate.prototype.previousState = function () {
        this.$router.go(-1);
    };
    RoleUpdate.prototype.initRelationships = function () {
        var _this = this;
        this.utilisateurService()
            .retrieve()
            .then(function (res) {
            _this.utilisateurs = res.data;
        });
    };
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('roleService'),
        __metadata("design:type", Function)
    ], RoleUpdate.prototype, "roleService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('alertService'),
        __metadata("design:type", Function)
    ], RoleUpdate.prototype, "alertService", void 0);
    __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Inject)('utilisateurService'),
        __metadata("design:type", Function)
    ], RoleUpdate.prototype, "utilisateurService", void 0);
    RoleUpdate = __decorate([
        (0,vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Component)({
            validations: validations,
        })
    ], RoleUpdate);
    return RoleUpdate;
}(vue_property_decorator__WEBPACK_IMPORTED_MODULE_0__.Vue));
/* harmony default export */ __webpack_exports__["default"] = (RoleUpdate);


/***/ }),

/***/ "./src/main/webapp/app/shared/model/enumerations/role.model.ts":
/*!*********************************************************************!*\
  !*** ./src/main/webapp/app/shared/model/enumerations/role.model.ts ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ROLE": function() { return /* binding */ ROLE; }
/* harmony export */ });
var ROLE;
(function (ROLE) {
    ROLE["Utilisateur"] = "Utilisateur";
    ROLE["Societaire"] = "Societaire";
    ROLE["DG"] = "DG";
    ROLE["Commercant"] = "Commercant";
    ROLE["Coursier"] = "Coursier";
})(ROLE || (ROLE = {}));


/***/ }),

/***/ "./src/main/webapp/app/shared/model/role.model.ts":
/*!********************************************************!*\
  !*** ./src/main/webapp/app/shared/model/role.model.ts ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Role": function() { return /* binding */ Role; }
/* harmony export */ });
var Role = /** @class */ (function () {
    function Role(id, role, utilisateur) {
        this.id = id;
        this.role = role;
        this.utilisateur = utilisateur;
    }
    return Role;
}());



/***/ }),

/***/ "./src/main/webapp/app/entities/role/role-update.vue":
/*!***********************************************************!*\
  !*** ./src/main/webapp/app/entities/role/role-update.vue ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./role-update.vue?vue&type=template&id=f3f6555c& */ "./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c&");
/* harmony import */ var _role_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./role-update.component.ts?vue&type=script&lang=ts& */ "./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _role_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__.render,
  _role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/main/webapp/app/entities/role/role-update.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts&":
/*!*********************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts& ***!
  \*********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_role_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./role-update.component.ts?vue&type=script&lang=ts& */ "./node_modules/ts-loader/index.js??clonedRuleSet-1.use[0]!./src/main/webapp/app/entities/role/role-update.component.ts?vue&type=script&lang=ts&");
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_ts_loader_index_js_clonedRuleSet_1_use_0_role_update_component_ts_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c&":
/*!******************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c& ***!
  \******************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__.render; },
/* harmony export */   "staticRenderFns": function() { return /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns; }
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_role_update_vue_vue_type_template_id_f3f6555c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./role-update.vue?vue&type=template&id=f3f6555c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./src/main/webapp/app/entities/role/role-update.vue?vue&type=template&id=f3f6555c& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; },
/* harmony export */   "staticRenderFns": function() { return /* binding */ staticRenderFns; }
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c,
    _setup = _vm._self._setupProxy
  return _c("div", { staticClass: "row justify-content-center" }, [
    _c("div", { staticClass: "col-8" }, [
      _c(
        "form",
        {
          attrs: { name: "editForm", role: "form", novalidate: "" },
          on: {
            submit: function ($event) {
              $event.preventDefault()
              return _vm.save()
            },
          },
        },
        [
          _c(
            "h2",
            {
              attrs: {
                id: "blogApp.role.home.createOrEditLabel",
                "data-cy": "RoleCreateUpdateHeading",
              },
              domProps: {
                textContent: _vm._s(
                  _vm.$t("blogApp.role.home.createOrEditLabel")
                ),
              },
            },
            [_vm._v("\n        Create or edit a Role\n      ")]
          ),
          _vm._v(" "),
          _c("div", [
            _vm.role.id
              ? _c("div", { staticClass: "form-group" }, [
                  _c(
                    "label",
                    {
                      attrs: { for: "id" },
                      domProps: {
                        textContent: _vm._s(_vm.$t("global.field.id")),
                      },
                    },
                    [_vm._v("ID")]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.role.id,
                        expression: "role.id",
                      },
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "id", name: "id", readonly: "" },
                    domProps: { value: _vm.role.id },
                    on: {
                      input: function ($event) {
                        if ($event.target.composing) return
                        _vm.$set(_vm.role, "id", $event.target.value)
                      },
                    },
                  }),
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "role-role" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.role.role")),
                  },
                },
                [_vm._v("Role")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.$v.role.role.$model,
                      expression: "$v.role.role.$model",
                    },
                  ],
                  staticClass: "form-control",
                  class: {
                    valid: !_vm.$v.role.role.$invalid,
                    invalid: _vm.$v.role.role.$invalid,
                  },
                  attrs: {
                    name: "role",
                    id: "role-role",
                    "data-cy": "role",
                    required: "",
                  },
                  on: {
                    change: function ($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function (o) {
                          return o.selected
                        })
                        .map(function (o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.$v.role.role,
                        "$model",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    },
                  },
                },
                _vm._l(_vm.rOLEValues, function (rOLE) {
                  return _c(
                    "option",
                    {
                      key: rOLE,
                      attrs: { label: _vm.$t("blogApp.ROLE." + rOLE) },
                      domProps: { value: rOLE },
                    },
                    [
                      _vm._v(
                        "\n              " + _vm._s(rOLE) + "\n            "
                      ),
                    ]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _vm.$v.role.role.$anyDirty && _vm.$v.role.role.$invalid
                ? _c("div", [
                    !_vm.$v.role.role.required
                      ? _c(
                          "small",
                          {
                            staticClass: "form-text text-danger",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$t("entity.validation.required")
                              ),
                            },
                          },
                          [
                            _vm._v(
                              "\n              This field is required.\n            "
                            ),
                          ]
                        )
                      : _vm._e(),
                  ])
                : _vm._e(),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "form-control-label",
                  attrs: { for: "role-utilisateur" },
                  domProps: {
                    textContent: _vm._s(_vm.$t("blogApp.role.utilisateur")),
                  },
                },
                [_vm._v("Utilisateur")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.role.utilisateur,
                      expression: "role.utilisateur",
                    },
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "role-utilisateur",
                    "data-cy": "utilisateur",
                    name: "utilisateur",
                  },
                  on: {
                    change: function ($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function (o) {
                          return o.selected
                        })
                        .map(function (o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.role,
                        "utilisateur",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    },
                  },
                },
                [
                  _c("option", { domProps: { value: null } }),
                  _vm._v(" "),
                  _vm._l(_vm.utilisateurs, function (utilisateurOption) {
                    return _c(
                      "option",
                      {
                        key: utilisateurOption.id,
                        domProps: {
                          value:
                            _vm.role.utilisateur &&
                            utilisateurOption.id === _vm.role.utilisateur.id
                              ? _vm.role.utilisateur
                              : utilisateurOption,
                        },
                      },
                      [
                        _vm._v(
                          "\n              " +
                            _vm._s(utilisateurOption.id) +
                            "\n            "
                        ),
                      ]
                    )
                  }),
                ],
                2
              ),
            ]),
          ]),
          _vm._v(" "),
          _c("div", [
            _c(
              "button",
              {
                staticClass: "btn btn-secondary",
                attrs: {
                  type: "button",
                  id: "cancel-save",
                  "data-cy": "entityCreateCancelButton",
                },
                on: {
                  click: function ($event) {
                    return _vm.previousState()
                  },
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "ban" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.cancel")),
                    },
                  },
                  [_vm._v("Cancel")]
                ),
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: {
                  type: "submit",
                  id: "save-entity",
                  "data-cy": "entityCreateSaveButton",
                  disabled: _vm.$v.role.$invalid || _vm.isSaving,
                },
              },
              [
                _c("font-awesome-icon", { attrs: { icon: "save" } }),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    domProps: {
                      textContent: _vm._s(_vm.$t("entity.action.save")),
                    },
                  },
                  [_vm._v("Save")]
                ),
              ],
              1
            ),
          ]),
        ]
      ),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_role_role-update_vue.180a1339565d8a94aebe.chunk.js.map