package info4.gl.blog.domain;

import info4.gl.blog.domain.enumeration.Etat;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Commande.class)
public abstract class Commande_ {

	public static volatile SingularAttribute<Commande, ZonedDateTime> date;
	public static volatile SingularAttribute<Commande, Long> idClient;
	public static volatile SingularAttribute<Commande, Long> idCourse;
	public static volatile SingularAttribute<Commande, Paiement> paiement;
	public static volatile SingularAttribute<Commande, Integer> prix;
	public static volatile SingularAttribute<Commande, Etat> etat;
	public static volatile SingularAttribute<Commande, Long> idLivreur;
	public static volatile SingularAttribute<Commande, Cooperative> cooperative;
	public static volatile SetAttribute<Commande, ProduitCommande> produitCommandes;
	public static volatile SingularAttribute<Commande, Utilisateur> utilisateur;
	public static volatile SingularAttribute<Commande, Course> course;
	public static volatile SingularAttribute<Commande, Long> id;
	public static volatile SingularAttribute<Commande, Long> idCommande;
	public static volatile SingularAttribute<Commande, Long> idCooperative;

	public static final String DATE = "date";
	public static final String ID_CLIENT = "idClient";
	public static final String ID_COURSE = "idCourse";
	public static final String PAIEMENT = "paiement";
	public static final String PRIX = "prix";
	public static final String ETAT = "etat";
	public static final String ID_LIVREUR = "idLivreur";
	public static final String COOPERATIVE = "cooperative";
	public static final String PRODUIT_COMMANDES = "produitCommandes";
	public static final String UTILISATEUR = "utilisateur";
	public static final String COURSE = "course";
	public static final String ID = "id";
	public static final String ID_COMMANDE = "idCommande";
	public static final String ID_COOPERATIVE = "idCooperative";

}

