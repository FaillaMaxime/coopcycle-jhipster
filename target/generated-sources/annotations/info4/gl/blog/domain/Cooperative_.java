package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cooperative.class)
public abstract class Cooperative_ {

	public static volatile SetAttribute<Cooperative, Commande> commandes;
	public static volatile SingularAttribute<Cooperative, String> adresse;
	public static volatile SingularAttribute<Cooperative, Long> id;
	public static volatile SetAttribute<Cooperative, Menu> menus;
	public static volatile SingularAttribute<Cooperative, FaitParti> faitParti;
	public static volatile SingularAttribute<Cooperative, String> nom;

	public static final String COMMANDES = "commandes";
	public static final String ADRESSE = "adresse";
	public static final String ID = "id";
	public static final String MENUS = "menus";
	public static final String FAIT_PARTI = "faitParti";
	public static final String NOM = "nom";

}

