package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Course.class)
public abstract class Course_ {

	public static volatile SingularAttribute<Course, Long> idCourse;
	public static volatile SingularAttribute<Course, Long> idCoursier;
	public static volatile SingularAttribute<Course, Coursier> coursier;
	public static volatile SingularAttribute<Course, String> adresse;
	public static volatile SingularAttribute<Course, Long> id;
	public static volatile SingularAttribute<Course, Commande> commande;

	public static final String ID_COURSE = "idCourse";
	public static final String ID_COURSIER = "idCoursier";
	public static final String COURSIER = "coursier";
	public static final String ADRESSE = "adresse";
	public static final String ID = "id";
	public static final String COMMANDE = "commande";

}

