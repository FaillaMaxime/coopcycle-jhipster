package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Coursier.class)
public abstract class Coursier_ {

	public static volatile SetAttribute<Coursier, Course> courses;
	public static volatile SingularAttribute<Coursier, String> vehicule;
	public static volatile SingularAttribute<Coursier, String> telephone;
	public static volatile SingularAttribute<Coursier, Long> id;
	public static volatile SingularAttribute<Coursier, String> nom;
	public static volatile SingularAttribute<Coursier, String> prenom;

	public static final String COURSES = "courses";
	public static final String VEHICULE = "vehicule";
	public static final String TELEPHONE = "telephone";
	public static final String ID = "id";
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";

}

