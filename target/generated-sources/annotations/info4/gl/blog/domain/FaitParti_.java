package info4.gl.blog.domain;

import info4.gl.blog.domain.enumeration.GERE;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FaitParti.class)
public abstract class FaitParti_ {

	public static volatile SingularAttribute<FaitParti, GERE> gere;
	public static volatile SingularAttribute<FaitParti, Utilisateur> utilisateur;
	public static volatile SingularAttribute<FaitParti, Long> id;
	public static volatile SingularAttribute<FaitParti, Cooperative> cooperative;

	public static final String GERE = "gere";
	public static final String UTILISATEUR = "utilisateur";
	public static final String ID = "id";
	public static final String COOPERATIVE = "cooperative";

}

