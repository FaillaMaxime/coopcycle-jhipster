package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Menu.class)
public abstract class Menu_ {

	public static volatile SetAttribute<Menu, Produit> produits;
	public static volatile SingularAttribute<Menu, Long> idMenu;
	public static volatile SingularAttribute<Menu, Long> id;
	public static volatile SingularAttribute<Menu, Long> idCooperative;
	public static volatile SingularAttribute<Menu, Cooperative> cooperative;

	public static final String PRODUITS = "produits";
	public static final String ID_MENU = "idMenu";
	public static final String ID = "id";
	public static final String ID_COOPERATIVE = "idCooperative";
	public static final String COOPERATIVE = "cooperative";

}

