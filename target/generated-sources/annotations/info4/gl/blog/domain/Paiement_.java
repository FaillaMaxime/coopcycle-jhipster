package info4.gl.blog.domain;

import info4.gl.blog.domain.enumeration.MethodeDePaiement;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Paiement.class)
public abstract class Paiement_ {

	public static volatile SingularAttribute<Paiement, MethodeDePaiement> methodeDePaiement;
	public static volatile SingularAttribute<Paiement, Long> idPaiement;
	public static volatile SingularAttribute<Paiement, Long> id;
	public static volatile SingularAttribute<Paiement, Commande> commande;
	public static volatile SingularAttribute<Paiement, Long> idCommande;

	public static final String METHODE_DE_PAIEMENT = "methodeDePaiement";
	public static final String ID_PAIEMENT = "idPaiement";
	public static final String ID = "id";
	public static final String COMMANDE = "commande";
	public static final String ID_COMMANDE = "idCommande";

}

