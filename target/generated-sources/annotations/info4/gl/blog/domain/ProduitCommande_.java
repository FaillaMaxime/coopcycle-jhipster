package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProduitCommande.class)
public abstract class ProduitCommande_ {

	public static volatile SingularAttribute<ProduitCommande, Boolean> quantitePossible;
	public static volatile SingularAttribute<ProduitCommande, Long> idProduit;
	public static volatile SingularAttribute<ProduitCommande, Long> id;
	public static volatile SingularAttribute<ProduitCommande, Commande> commande;
	public static volatile SingularAttribute<ProduitCommande, Long> idCommande;
	public static volatile SingularAttribute<ProduitCommande, Integer> quantite;
	public static volatile SetAttribute<ProduitCommande, Produit> products;

	public static final String QUANTITE_POSSIBLE = "quantitePossible";
	public static final String ID_PRODUIT = "idProduit";
	public static final String ID = "id";
	public static final String COMMANDE = "commande";
	public static final String ID_COMMANDE = "idCommande";
	public static final String QUANTITE = "quantite";
	public static final String PRODUCTS = "products";

}

