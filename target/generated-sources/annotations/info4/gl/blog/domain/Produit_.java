package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Produit.class)
public abstract class Produit_ {

	public static volatile SingularAttribute<Produit, Long> idProduit;
	public static volatile SingularAttribute<Produit, Float> prix;
	public static volatile SingularAttribute<Produit, Integer> disponibilite;
	public static volatile SingularAttribute<Produit, Long> idMenu;
	public static volatile SingularAttribute<Produit, Long> id;
	public static volatile SingularAttribute<Produit, Menu> menu;
	public static volatile SingularAttribute<Produit, String> nom;
	public static volatile SetAttribute<Produit, ProduitCommande> produitcommandes;

	public static final String ID_PRODUIT = "idProduit";
	public static final String PRIX = "prix";
	public static final String DISPONIBILITE = "disponibilite";
	public static final String ID_MENU = "idMenu";
	public static final String ID = "id";
	public static final String MENU = "menu";
	public static final String NOM = "nom";
	public static final String PRODUITCOMMANDES = "produitcommandes";

}

