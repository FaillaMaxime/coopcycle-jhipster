package info4.gl.blog.domain;

import info4.gl.blog.domain.enumeration.ROLE;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Role.class)
public abstract class Role_ {

	public static volatile SingularAttribute<Role, ROLE> role;
	public static volatile SingularAttribute<Role, Utilisateur> utilisateur;
	public static volatile SingularAttribute<Role, Long> id;

	public static final String ROLE = "role";
	public static final String UTILISATEUR = "utilisateur";
	public static final String ID = "id";

}

