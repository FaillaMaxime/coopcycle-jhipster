package info4.gl.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Utilisateur.class)
public abstract class Utilisateur_ {

	public static volatile SetAttribute<Utilisateur, Commande> commandes;
	public static volatile SetAttribute<Utilisateur, Role> roles;
	public static volatile SingularAttribute<Utilisateur, String> adresse;
	public static volatile SingularAttribute<Utilisateur, String> telephone;
	public static volatile SingularAttribute<Utilisateur, Long> id;
	public static volatile SetAttribute<Utilisateur, FaitParti> faitPartis;
	public static volatile SingularAttribute<Utilisateur, String> nom;
	public static volatile SingularAttribute<Utilisateur, String> prenom;

	public static final String COMMANDES = "commandes";
	public static final String ROLES = "roles";
	public static final String ADRESSE = "adresse";
	public static final String TELEPHONE = "telephone";
	public static final String ID = "id";
	public static final String FAIT_PARTIS = "faitPartis";
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";

}

